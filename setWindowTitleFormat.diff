From: Tony Balinski <ajbj@free.fr>
Subject: Allow a macro to set the document's window title or title format

This is done by allowing the document window to carry a custom version
of the title format string. The new macro function,
set_window_title_format(), is called as follows:

	set_window_title_format(string)
	set_window_title_format(string, "text")
	set_window_title_format(string, "format")

The string will be interpreted as a title format if the "format" keyword is
supplied, or as a fixed title string otherwise. The format directives are
those used in Preferences> Default Settings> Customize Window Title...
dialog. If an empty string is used, the default format (from the
Preferences) is applied.

---

 doc/help.etx           |    7 ++++
 source/built-ins.h     |    1 
 source/highlightData.c |    2 -
 source/macro.c         |   74 +++++++++++++++++++++++++++++++++++++++++++++++++
 source/nedit.h         |    1 
 source/window.c        |   25 ++++++++++++----
 6 files changed, 103 insertions(+), 7 deletions(-)

diff --quilt old/doc/help.etx new/doc/help.etx
--- old/doc/help.etx
+++ new/doc/help.etx
@@ -4051,9 +4051,16 @@ Preferences
   below).
 
 **Customize Window Title**
+.. ? help~
+  Opens a dialog where the information to be displayed in the window's title
+  field can be defined and tested. The Customize_Window_Title_Dialog_ contains
+  a Help button, providing further information about the options available.
+.. ~ help
+.. ! help~
   Opens a dialog where the information to be displayed in the window's title
   field can be defined and tested. The dialog contains a Help button, providing
   further information about the options available.
+.. ~ help
 
 **Searching**
   Options for controlling the behavior of Find and Replace commands:
diff --quilt old/source/macro.c new/source/macro.c
--- old/source/macro.c
+++ new/source/macro.c
@@ -3442,6 +3442,80 @@ static int defineMS(WindowInfo *window, 
     return True;
 }
 
+/*
+ * set_window_title(format[, ("text"|"format")])
+ */
+static int setWindowTitleMS(WindowInfo *window, DataValue *argList,
+        int nArgs, DataValue *result, char **errMsg)
+{
+    char stringStorage[2][TYPE_INT_STR_SIZE(int)];
+    char *fmt = NULL;
+    char *type = NULL;
+    char *newFmt;
+    char *from, *to;
+    int isText = 1;
+    int perCents;
+
+    if (nArgs > 2) {
+        *errMsg = "subroutine %s called with too many arguments";
+        return False;
+    }
+    if (nArgs > 0 &&
+        !readStringArg(argList[0], &fmt, stringStorage[0], errMsg)) {
+        return False;
+    }
+    if (nArgs > 1 &&
+        !readStringArg(argList[1], &type, stringStorage[1], errMsg)) {
+        return False;
+    }
+    if (type) {
+        if (strcmp(type, "text") == 0)
+            isText = 1;
+        else if (strcmp(type, "format") == 0)
+            isText = 0;
+        else {
+            *errMsg = "subroutine %s type value must be \"text\" or \"format\"";
+            return False;
+        }
+    }
+
+    if (!fmt || !*fmt) {
+        /* empty string: return tu default behaviour */
+        XtFree(window->titleFormat);
+        window->titleFormat = NULL;
+        UpdateWindowTitle(window);
+        return True;
+    }
+
+    perCents = 0;
+    if (isText) {
+        /* measure extra space for '%' */
+        for (from = fmt; *from; from++)
+            if ('%' == *from)
+                ++perCents;
+    }
+
+    newFmt = XtMalloc(strlen(fmt) + perCents + 1);
+    if (!newFmt) {
+        *errMsg = "subroutine %s failed to allocate format value";
+        return False;
+    }
+    if (isText) {
+        /* double up any % signs */
+        for (from = fmt, to = newFmt; *from; *to++ = *from++)
+            if ('%' == *from)
+                *to++ = '%';
+        *to = '\0';
+    }
+    else
+        strcpy(newFmt, fmt);
+
+    XtFree(window->titleFormat);
+    window->titleFormat = newFmt;
+    UpdateWindowTitle(window);
+    return True;
+}
+
 /* T Balinski */
 static int listDialogMS(WindowInfo *window, DataValue *argList, int nArgs,
       DataValue *result, char **errMsg)
diff --quilt old/source/nedit.h new/source/nedit.h
--- old/source/nedit.h
+++ new/source/nedit.h
@@ -459,6 +459,7 @@ typedef struct _WindowInfo {
 #endif
     char	filename[MAXPATHLEN];	/* name component of file being edited*/
     char	path[MAXPATHLEN];	/* path component of file being edited*/
+    char        *titleFormat;           /* custom title format string */
     unsigned	fileMode;		/* permissions of file being edited */
     uid_t	fileUid; 		/* last recorded user id of the file */
     gid_t	fileGid;		/* last recorded group id of the file */
diff --quilt old/source/window.c new/source/window.c
--- old/source/window.c
+++ new/source/window.c
@@ -254,6 +254,7 @@ WindowInfo *CreateWindow(const char *nam
     window->writableWindows = NULL;
     window->nWritableWindows = 0;
     window->fileChanged = FALSE;
+    window->titleFormat = NULL;
     window->fileMode = 0;
     window->fileUid = 0;
     window->fileGid = 0;
@@ -983,7 +984,13 @@ void CloseWindow(WindowInfo *window)
     /* Kill shell sub-process and free related memory */
     AbortShellCommand(window);
 #endif /*VMS*/
-    
+
+    /* drop any custom title */
+    if (window->titleFormat) {
+        XtFree(window->titleFormat);
+        window->titleFormat = NULL;
+    }
+
     /* Unload the default tips files for this language mode if necessary */
     UnloadLanguageModeTipsFile(window);
 
@@ -2099,11 +2106,13 @@ void SetWindowModified(WindowInfo *windo
 */
 void UpdateWindowTitle(const WindowInfo *window)
 {
-    char *iconTitle, *title;
+    char *iconTitle, *title, *format, *filename;
     
     if (!IsTopDocument(window))
     	return;
 
+    format = window->titleFormat ? window->titleFormat : GetPrefTitleFormat();
+    filename = window->filename;
     title = FormatWindowTitle(window->filename,
                                     window->path,
 #ifdef VMS
@@ -2117,11 +2126,14 @@ void UpdateWindowTitle(const WindowInfo 
                                     window->lockReasons,
                                     window->fileChanged,
                                     window->transient,
-                                    GetPrefTitleFormat());
-                   
-    iconTitle = XtMalloc(strlen(window->filename) + 2); /* strlen("*")+1 */
+                              format);
+
+    if (strcmp(title, format) == 0)
+        filename = title;
+
+    iconTitle = XtMalloc(strlen(filename) + 2); /* strlen("*")+1 */
 
-    strcpy(iconTitle, window->filename);
+    strcpy(iconTitle, filename);
     if (window->fileChanged && !window->transient)
         strcat(iconTitle, "*");
     XtVaSetValues(window->shell, XmNtitle, title, XmNiconName, iconTitle, NULL);
@@ -3424,6 +3436,7 @@ WindowInfo* CreateDocument(WindowInfo* s
     window->nWritableWindows = 0;
     window->fileChanged = FALSE;
     window->fileMissing = True;
+    window->titleFormat = NULL;
     window->fileMode = 0;
     window->fileUid = 0;
     window->fileGid = 0;
diff --quilt old/source/highlightData.c new/source/highlightData.c
--- old/source/highlightData.c
+++ new/source/highlightData.c
@@ -551,7 +551,7 @@ static char *DefaultPatternSets[] = {
         Built-in Misc Vars:\"(?<!\\Y)\\$(?:active_pane|calltip_ID|column|cursor|display_width|empty_array|file_name|file_path|language_mode|line|locked|max_font_width|min_font_width|modified|n_display_lines|n_panes|rangeset_list|read_only|selection_(?:start|end|left|right)|server_name|text_length|top_line|transient|VERSION|NEDIT_HOME)>\":::Identifier::\n\
         Built-in Pref Vars:\"(?<!\\Y)\\$(?:auto_indent|em_tab_dist|file_format|font_name|font_name_bold|font_name_bold_italic|font_name_italic|highlight_syntax|incremental_backup|incremental_search_line|make_backup_copy|match_syntax_based|overtype_mode|show_line_numbers|show_matching|statistics_line|tab_dist|use_tabs|wrap_margin|wrap_text)>\":::Identifier2::\n\
         Built-in Special Vars:\"(?<!\\Y)\\$(?:args|[1-9]|list_dialog_button|n_args|read_status|search_end|shell_cmd_status|string_dialog_button|sub_sep)>\":::String1::\n\
-        Built-in Subrs:\"<(?:args|append_file|beep|call|calltip|clipboard_to_string|define|dialog|filename_dialog|dict_(?:insert|complete|save|append|is_element)|focus_window|get_character|get_matching|get_pattern_(by_name|at_pos)|get_range|get_selection|get_style_(by_name|at_pos)|getenv|highlight_calltip_line|kill_calltip|length|list_dialog|max|min|n_args|rangeset_(?:add|create|destroy|get_by_name|includes|info|invert|range|set_color|set_mode|set_name|subtract)|read_file|replace_in_string|replace_range|replace_selection|replace_substring|search|search_string|select|select_rectangle|set_cursor_pos|set_transient|shell_command|split|string_compare|string_dialog|string_to_clipboard|substring|t_print|to_(?:column|line|pos)|tolower|toupper|valid_number|write_file)(?=\\s*\\()\":::Subroutine::\n\
+        Built-in Subrs:\"<(?:args|append_file|beep|call|calltip|clipboard_to_string|define|dialog|filename_dialog|dict_(?:insert|complete|save|append|is_element)|focus_window|get_character|get_matching|get_pattern_(by_name|at_pos)|get_range|get_selection|get_style_(by_name|at_pos)|getenv|highlight_calltip_line|kill_calltip|length|list_dialog|max|min|n_args|rangeset_(?:add|create|destroy|get_by_name|includes|info|invert|range|set_color|set_mode|set_name|subtract)|read_file|replace_in_string|replace_range|replace_selection|replace_substring|search|search_string|select|select_rectangle|set_cursor_pos|set_transient|set_window_title|shell_command|split|string_compare|string_dialog|string_to_clipboard|substring|t_print|to_(?:column|line|pos)|tolower|toupper|valid_number|write_file)(?=\\s*\\()\":::Subroutine::\n\
         Menu Actions:\"<(?:new(?:_tab|_opposite)?|open|open-dialog|open_dialog|open-selected|open_selected|close|save|save-as|save_as|save-as-dialog|save_as_dialog|revert-to-saved|revert_to_saved|revert_to_saved_dialog|include-file|include_file|include-file-dialog|include_file_dialog|load-macro-file|load_macro_file|load-macro-file-dialog|load_macro_file_dialog|load-tags-file|load_tags_file|load-tags-file-dialog|load_tags_file_dialog|unload_tags_file|load_tips_file|load_tips_file_dialog|unload_tips_file|print|print-selection|print_selection|exit|undo|redo|delete|select-all|select_all|shift-left|shift_left|shift-left-by-tab|shift_left_by_tab|shift-right|shift_right|shift-right-by-tab|shift_right_by_tab|find|find-dialog|find_dialog|find-again|find_again|find-selection|find_selection|find_incremental|start_incremental_find|replace|replace-dialog|replace_dialog|replace-all|replace_all|replace-in-selection|replace_in_selection|replace-again|replace_again|replace_find|replace_find_same|replace_find_again|goto-line-number|goto_line_number|goto-line-number-dialog|goto_line_number_dialog|goto-selected|goto_selected|mark|mark-dialog|mark_dialog|goto-mark|goto_mark|goto-mark-dialog|goto_mark_dialog|match|select_to_matching|goto_matching|find-definition|find_definition|show_tip|split-pane|split_pane|close-pane|close_pane|detach_document(?:_dialog)?|move_document_dialog|(?:next|previous|last)_document|uppercase|lowercase|fill-paragraph|fill_paragraph|control-code-dialog|control_code_dialog|filter-selection-dialog|filter_selection_dialog|filter-selection|filter_selection|execute-command|execute_command|execute-command-dialog|execute_command_dialog|execute-command-line|execute_command_line|shell-menu-command|shell_menu_command|macro-menu-command|macro_menu_command|bg_menu_command|post_window_bg_menu|post_tab_context_menu|beginning-of-selection|beginning_of_selection|end-of-selection|end_of_selection|repeat_macro|repeat_dialog|raise_window|focus_pane|set_statistics_line|set_incremental_search_line|set_show_line_numbers|set_auto_indent|set_wrap_text|set_wrap_margin|set_highlight_syntax|set_make_backup_copy|set_incremental_backup|set_show_matching|set_match_syntax_based|set_overtype_mode|set_locked|set_tab_dist|set_em_tab_dist|set_use_tabs|set_fonts|set_language_mode)(?=\\s*\\()\":::Subroutine::\n\
         Text Actions:\"<(?:self-insert|self_insert|grab-focus|grab_focus|extend-adjust|extend_adjust|extend-start|extend_start|extend-end|extend_end|secondary-adjust|secondary_adjust|secondary-or-drag-adjust|secondary_or_drag_adjust|secondary-start|secondary_start|secondary-or-drag-start|secondary_or_drag_start|process-bdrag|process_bdrag|move-destination|move_destination|move-to|move_to|move-to-or-end-drag|move_to_or_end_drag|end_drag|copy-to|copy_to|copy-to-or-end-drag|copy_to_or_end_drag|exchange|process-cancel|process_cancel|paste-clipboard|paste_clipboard|copy-clipboard|copy_clipboard|cut-clipboard|cut_clipboard|copy-primary|copy_primary|cut-primary|cut_primary|newline|newline-and-indent|newline_and_indent|newline-no-indent|newline_no_indent|delete-selection|delete_selection|delete-previous-character|delete_previous_character|delete-next-character|delete_next_character|delete-previous-word|delete_previous_word|delete-next-word|delete_next_word|delete-to-start-of-line|delete_to_start_of_line|delete-to-end-of-line|delete_to_end_of_line|forward-character|forward_character|backward-character|backward_character|key-select|key_select|process-up|process_up|process-down|process_down|process-shift-up|process_shift_up|process-shift-down|process_shift_down|process-home|process_home|forward-word|forward_word|backward-word|backward_word|forward-paragraph|forward_paragraph|backward-paragraph|backward_paragraph|beginning-of-line|beginning_of_line|end-of-line|end_of_line|beginning-of-file|beginning_of_file|end-of-file|end_of_file|next-page|next_page|previous-page|previous_page|page-left|page_left|page-right|page_right|toggle-overstrike|toggle_overstrike|scroll-up|scroll_up|scroll-down|scroll_down|scroll_left|scroll_right|scroll-to-line|scroll_to_line|select-all|select_all|deselect-all|deselect_all|focusIn|focusOut|process-return|process_return|process-tab|process_tab|insert-string|insert_string|mouse_pan)(?=\\s*\\()\":::Subroutine::\n\
         Macro Hooks:\"<(?:(?:pre|post)_(?:open|save)|cursor_moved|modified|(?:losing_)?focus|language_mode)_hook(?=\\s*\\()\":::Subroutine1::\n\
diff --quilt old/source/built-ins.h new/source/built-ins.h
--- old/source/built-ins.h
+++ new/source/built-ins.h
@@ -72,6 +72,7 @@ MS(define, define)
 MS(to_pos, toPos)
 MS(to_line, toLine)
 MS(to_column, toColumn)
+MS(set_window_title, setWindowTitle)
 
 MV(cursor, cursor)
 MV(line, line)
