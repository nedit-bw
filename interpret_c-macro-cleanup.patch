Subject: interpret.c: macro cleanup 

Use 'do { } while (0)' syntax for macros and consolidate num/string conversion.

---

 source/interpret.c |  410 +++++++++++++++++++++++++++++++----------------------
 1 file changed, 241 insertions(+), 169 deletions(-)

diff --quilt old/source/interpret.c new/source/interpret.c
--- old/source/interpret.c
+++ new/source/interpret.c
@@ -136,6 +136,8 @@ static int arrayEntryCompare(rbTreeNode 
 static void arrayDisposeNode(rbTreeNode *src);
 static SparseArrayEntry *allocateSparseArrayEntry(void);
 
+static const char *tagToStr(enum typeTags tag);
+
 /*#define DEBUG_ASSEMBLY*/
 /*#define DEBUG_STACK*/
 
@@ -177,7 +179,7 @@ static SparseArrayEntryWrapper *Allocate
    the macros are used */
 static const char *StackOverflowMsg = "macro stack overflow";
 static const char *StackUnderflowMsg = "macro stack underflow";
-static const char *StringToNumberMsg = "string could not be converted to number";
+static const char *StringToNumberMsg = "string '%s' could not be converted to number";
 
 /* Temporary global data for use while accumulating programs */
 static Symbol *LocalSymList = NULL;	 /* symbols local to the program */
@@ -1128,98 +1130,152 @@ static void freeSymbolTable(Symbol *symT
     }    
 }
 
+/* true, if you can pop n values */
+#define OK_TO_POP(n) \
+    ((StackP - (n)) >= TheStack)
+
+#define POP_CHECK(n) \
+    do { \
+        if (!OK_TO_POP(n)) { \
+            return execError(StackUnderflowMsg, ""); \
+        } \
+    } while (0)
+
+/* true, if you can push n values */
+#define OK_TO_PUSH(n) \
+    (StackP + (n) <= &TheStack[STACK_SIZE])
+
+ #define PUSH_CHECK(n) \
+    do { \
+        if (!OK_TO_PUSH(n)) { \
+            return execError(StackOverflowMsg, ""); \
+        } \
+    } while (0)
+
+#define PEEK_CHECK(n) \
+    do { \
+        if (!OK_TO_POP((n) + 1)) { \
+            return execError(StackUnderflowMsg, ""); \
+        } \
+        if (!OK_TO_PUSH(-(n))) { \
+            return execError(StackOverflowMsg, ""); \
+        } \
+    } while (0)
+
 #define POP(dataVal) \
-    if (StackP == TheStack) \
-	return execError(StackUnderflowMsg, ""); \
-    dataVal = *--StackP;
+    do { \
+        POP_CHECK(1); \
+        dataVal = *--StackP; \
+    } while (0)
    
 #define PUSH(dataVal) \
-    if (StackP >= &TheStack[STACK_SIZE]) \
-    	return execError(StackOverflowMsg, ""); \
-    *StackP++ = dataVal;
+    do { \
+        PUSH_CHECK(1); \
+        *StackP++ = dataVal; \
+    } while (0)
 
 #define PEEK(dataVal, peekIndex) \
-    dataVal = *(StackP - peekIndex - 1);
+    do { \
+        PEEK_CHECK(peekIndex); \
+        dataVal = *(StackP - (peekIndex) - 1); \
+    } while (0)
+
+#define TO_INT(dataVal, number) \
+    do { \
+        int __int; \
+        if (dataVal.tag == INT_TAG) { \
+            __int = dataVal.val.n; \
+        } else if (dataVal.tag == STRING_TAG) { \
+            if (!StringToNum(dataVal.val.str.rep, &__int)) {\
+                return execError(StringToNumberMsg, dataVal.val.str.rep); \
+            } \
+        } else { \
+            return execError("incompatible type in integer context: <%s>", \
+                    tagToStr(dataVal.tag)); \
+        } \
+        number = __int; \
+    } while (0)
+
+#define TO_STRING(dataVal, string) \
+    do { \
+        char *__str; \
+        if (dataVal.tag == STRING_TAG) { \
+            __str = dataVal.val.str.rep; \
+        } else if (dataVal.tag == INT_TAG) { \
+            __str = AllocString(TYPE_INT_STR_SIZE(int)); \
+            sprintf(__str, "%d", dataVal.val.n); \
+        } else { \
+            return execError("incompatible type in string context: <%s>", \
+                    tagToStr(dataVal.tag)); \
+        } \
+        string = __str; \
+    } while (0)
 
 #define POP_INT(number) \
-    if (StackP == TheStack) \
-	return execError(StackUnderflowMsg, ""); \
-    --StackP; \
-    if (StackP->tag == STRING_TAG) { \
-    	if (!StringToNum(StackP->val.str.rep, &number)) \
-    	    return execError(StringToNumberMsg, ""); \
-    } else if (StackP->tag == INT_TAG) \
-        number = StackP->val.n; \
-    else \
-        return(execError("can't convert array to integer", NULL));
+    do { \
+        DataValue dv; \
+        POP(dv); \
+        TO_INT(dv, number); \
+    } while (0)
 
 #define POP_STRING(string) \
-    if (StackP == TheStack) \
-	return execError(StackUnderflowMsg, ""); \
-    --StackP; \
-    if (StackP->tag == INT_TAG) { \
-    	string = AllocString(TYPE_INT_STR_SIZE(int)); \
-    	sprintf(string, "%d", StackP->val.n); \
-    } else if (StackP->tag == STRING_TAG) \
-        string = StackP->val.str.rep; \
-    else \
-        return(execError("can't convert array to string", NULL));
-   
-#define PEEK_STRING(string, peekIndex) \
-    if ((StackP - peekIndex - 1)->tag == INT_TAG) { \
-        string = AllocString(TYPE_INT_STR_SIZE(int)); \
-        sprintf(string, "%d", (StackP - peekIndex - 1)->val.n); \
-    } \
-    else if ((StackP - peekIndex - 1)->tag == STRING_TAG) { \
-        string = (StackP - peekIndex - 1)->val.str.rep; \
-    } \
-    else { \
-        return(execError("can't convert array to string", NULL)); \
-    }
+    do { \
+        DataValue dv; \
+        POP(dv); \
+        TO_STRING(dv, string); \
+    } while (0)
 
 #define PEEK_INT(number, peekIndex) \
-    if ((StackP - peekIndex - 1)->tag == STRING_TAG) { \
-        if (!StringToNum((StackP - peekIndex - 1)->val.str.rep, &number)) { \
-    	    return execError(StringToNumberMsg, ""); \
-        } \
-    } else if ((StackP - peekIndex - 1)->tag == INT_TAG) { \
-        number = (StackP - peekIndex - 1)->val.n; \
-    } \
-    else { \
-        return(execError("can't convert array to string", NULL)); \
-    }
+    do { \
+        DataValue dv; \
+        PEEK(dv, peekIndex); \
+        TO_INT(dv, number); \
+    } while (0)
+
+#define PEEK_STRING(string, peekIndex) \
+    do { \
+        DataValue dv; \
+        PEEK(dv); \
+        TO_STRING(dv, string); \
+    } while (0)
 
 #define PUSH_INT(number) \
-    if (StackP >= &TheStack[STACK_SIZE]) \
-    	return execError(StackOverflowMsg, ""); \
-    StackP->tag = INT_TAG; \
-    StackP->val.n = number; \
-    StackP++;
-    
+    do { \
+        DataValue dv; \
+        dv.tag = INT_TAG; \
+        dv.val.n = (number); \
+        PUSH(dv); \
+    } while (0)
+
 #define PUSH_STRING(string, length) \
-    if (StackP >= &TheStack[STACK_SIZE]) \
-    	return execError(StackOverflowMsg, ""); \
-    StackP->tag = STRING_TAG; \
-    StackP->val.str.rep = string; \
-    StackP->val.str.len = length; \
-    StackP++;
+    do { \
+        DataValue dv; \
+        dv.tag = STRING_TAG; \
+        dv.val.str.rep = (string); \
+        dv.val.str.len = (length); \
+        PUSH(dv); \
+    } while (0)
 
 #define BINARY_NUMERIC_OPERATION(operator) \
-    int n1, n2; \
-    DISASM_RT(PC-1, 1); \
-    STACKDUMP(2, 3); \
-    POP_INT(n2) \
-    POP_INT(n1) \
-    PUSH_INT(n1 operator n2) \
-    return STAT_OK;
+    do { \
+        int n1, n2; \
+        DISASM_RT(PC-1, 1); \
+        STACKDUMP(2, 3); \
+        POP_INT(n2); \
+        POP_INT(n1); \
+        PUSH_INT(n1 operator n2); \
+        return STAT_OK; \
+    } while (0)
 
 #define UNARY_NUMERIC_OPERATION(operator) \
-    int n; \
-    DISASM_RT(PC-1, 1); \
-    STACKDUMP(1, 3); \
-    POP_INT(n) \
-    PUSH_INT(operator n) \
-    return STAT_OK;
+    do { \
+        int n; \
+        DISASM_RT(PC-1, 1); \
+        STACKDUMP(1, 3); \
+        POP_INT(n); \
+        PUSH_INT(operator n); \
+        return STAT_OK; \
+    } while (0)
 
 /*
 ** copy a symbol's value onto the stack
@@ -1269,7 +1325,7 @@ static int pushSymVal(void)
     	return execError("variable not set: %s", s->name);
     }
 
-    PUSH(symVal)
+    PUSH(symVal);
 
     return STAT_OK;
 }
@@ -1281,7 +1337,7 @@ static int pushArgVal(void)
     DISASM_RT(PC-1, 1);
     STACKDUMP(1, 3);
 
-    POP_INT(argNum)
+    POP_INT(argNum);
     --argNum;
     nArgs = FP_GET_ARG_COUNT(FrameP);
     if (argNum >= nArgs || argNum < 0) {
@@ -1372,7 +1428,7 @@ static int pushArraySymVal(void)
         return execError("variable not set: %s", sym->name);
     }
 
-    PUSH(*dataPtr)
+    PUSH(*dataPtr);
 
     return STAT_OK;
 }
@@ -1416,7 +1472,7 @@ static int assign(void)
         dataPtr = &sym->value;
     }
 
-    POP(value)
+    POP(value);
 
     if (value.tag == ARRAY_TAG) {
         ArrayCopy(dataPtr, &value);
@@ -1439,8 +1495,8 @@ static int dupStack(void)
     DISASM_RT(PC-1, 1);
     STACKDUMP(1, 3);
 
-    PEEK(value, 0)
-    PUSH(value)
+    PEEK(value, 0);
+    PUSH(value);
 
     return STAT_OK;
 }
@@ -1461,16 +1517,16 @@ static int add(void)
     DISASM_RT(PC-1, 1);
     STACKDUMP(2, 3);
 
-    PEEK(rightVal, 0)
+    PEEK(rightVal, 0);
     if (rightVal.tag == ARRAY_TAG) {
-        PEEK(leftVal, 1)
+        PEEK(leftVal, 1);
         if (leftVal.tag == ARRAY_TAG) {
             SparseArrayEntry *leftIter, *rightIter;
             resultArray.tag = ARRAY_TAG;
             resultArray.val.arrayPtr = ArrayNew();
             
-            POP(rightVal)
-            POP(leftVal)
+            POP(rightVal);
+            POP(leftVal);
             leftIter = arrayIterateFirst(&leftVal);
             rightIter = arrayIterateFirst(&rightVal);
             while (leftIter || rightIter) {
@@ -1504,16 +1560,16 @@ static int add(void)
                     return(execError("array insertion failure", NULL));
                 }
             }
-            PUSH(resultArray)
+            PUSH(resultArray);
         }
         else {
             return(execError("can't mix math with arrays and non-arrays", NULL));
         }
     }
     else {
-        POP_INT(n2)
-        POP_INT(n1)
-        PUSH_INT(n1 + n2)
+        POP_INT(n2);
+        POP_INT(n1);
+        PUSH_INT(n1 + n2);
     }
     return(STAT_OK);
 }
@@ -1533,16 +1589,16 @@ static int subtract(void)
     DISASM_RT(PC-1, 1);
     STACKDUMP(2, 3);
 
-    PEEK(rightVal, 0)
+    PEEK(rightVal, 0);
     if (rightVal.tag == ARRAY_TAG) {
-        PEEK(leftVal, 1)
+        PEEK(leftVal, 1);
         if (leftVal.tag == ARRAY_TAG) {
             SparseArrayEntry *leftIter, *rightIter;
             resultArray.tag = ARRAY_TAG;
             resultArray.val.arrayPtr = ArrayNew();
             
-            POP(rightVal)
-            POP(leftVal)
+            POP(rightVal);
+            POP(leftVal);
             leftIter = arrayIterateFirst(&leftVal);
             rightIter = arrayIterateFirst(&rightVal);
             while (leftIter) {
@@ -1570,16 +1626,16 @@ static int subtract(void)
                     return(execError("array insertion failure", NULL));
                 }
             }
-            PUSH(resultArray)
+            PUSH(resultArray);
         }
         else {
             return(execError("can't mix math with arrays and non-arrays", NULL));
         }
     }
     else {
-        POP_INT(n2)
-        POP_INT(n1)
-        PUSH_INT(n1 - n2)
+        POP_INT(n2);
+        POP_INT(n1);
+        PUSH_INT(n1 - n2);
     }
     return(STAT_OK);
 }
@@ -1595,7 +1651,7 @@ static int subtract(void)
 */
 static int multiply(void)
 {
-    BINARY_NUMERIC_OPERATION(*)
+    BINARY_NUMERIC_OPERATION(*);
 }
 
 static int divide(void)
@@ -1605,12 +1661,12 @@ static int divide(void)
     DISASM_RT(PC-1, 1);
     STACKDUMP(2, 3);
 
-    POP_INT(n2)
-    POP_INT(n1)
+    POP_INT(n2);
+    POP_INT(n1);
     if (n2 == 0) {
 	return execError("division by zero", "");
     }
-    PUSH_INT(n1 / n2)
+    PUSH_INT(n1 / n2);
     return STAT_OK;
 }
 
@@ -1621,48 +1677,48 @@ static int modulo(void)
     DISASM_RT(PC-1, 1);
     STACKDUMP(2, 3);
 
-    POP_INT(n2)
-    POP_INT(n1)
+    POP_INT(n2);
+    POP_INT(n1);
     if (n2 == 0) {
 	return execError("modulo by zero", "");
     }
-    PUSH_INT(n1 % n2)
+    PUSH_INT(n1 % n2);
     return STAT_OK;
 }
 
 static int negate(void)
 {
-    UNARY_NUMERIC_OPERATION(-)
+    UNARY_NUMERIC_OPERATION(-);
 }
 
 static int increment(void)
 {
-    UNARY_NUMERIC_OPERATION(++)
+    UNARY_NUMERIC_OPERATION(++);
 }
 
 static int decrement(void)
 {
-    UNARY_NUMERIC_OPERATION(--)
+    UNARY_NUMERIC_OPERATION(--);
 }
 
 static int gt(void)
 {
-    BINARY_NUMERIC_OPERATION(>)
+    BINARY_NUMERIC_OPERATION(>);
 }
 
 static int lt(void)
 {
-    BINARY_NUMERIC_OPERATION(<)
+    BINARY_NUMERIC_OPERATION(<);
 }
 
 static int ge(void)
 {
-    BINARY_NUMERIC_OPERATION(>=)
+    BINARY_NUMERIC_OPERATION(>=);
 }
 
 static int le(void)
 {
-    BINARY_NUMERIC_OPERATION(<=)
+    BINARY_NUMERIC_OPERATION(<=);
 }
 
 /*
@@ -1678,8 +1734,8 @@ static int eq(void)
     DISASM_RT(PC-1, 1);
     STACKDUMP(2, 3);
 
-    POP(v1)
-    POP(v2)
+    POP(v1);
+    POP(v2);
     if (v1.tag == INT_TAG && v2.tag == INT_TAG) {
         v1.val.n = v1.val.n == v2.val.n;
     }
@@ -1708,7 +1764,7 @@ static int eq(void)
         return(execError("incompatible types to compare", NULL));
     }
     v1.tag = INT_TAG;
-    PUSH(v1)
+    PUSH(v1);
     return(STAT_OK);
 }
 
@@ -1734,16 +1790,16 @@ static int bitAnd(void)
     DISASM_RT(PC-1, 1);
     STACKDUMP(2, 3);
 
-    PEEK(rightVal, 0)
+    PEEK(rightVal, 0);
     if (rightVal.tag == ARRAY_TAG) {
-        PEEK(leftVal, 1)
+        PEEK(leftVal, 1);
         if (leftVal.tag == ARRAY_TAG) {
             SparseArrayEntry *leftIter, *rightIter;
             resultArray.tag = ARRAY_TAG;
             resultArray.val.arrayPtr = ArrayNew();
             
-            POP(rightVal)
-            POP(leftVal)
+            POP(rightVal);
+            POP(leftVal);
             leftIter = arrayIterateFirst(&leftVal);
             rightIter = arrayIterateFirst(&rightVal);
             while (leftIter && rightIter) {
@@ -1765,16 +1821,16 @@ static int bitAnd(void)
                     return(execError("array insertion failure", NULL));
                 }
             }
-            PUSH(resultArray)
+            PUSH(resultArray);
         }
         else {
             return(execError("can't mix math with arrays and non-arrays", NULL));
         }
     }
     else {
-        POP_INT(n2)
-        POP_INT(n1)
-        PUSH_INT(n1 & n2)
+        POP_INT(n2);
+        POP_INT(n1);
+        PUSH_INT(n1 & n2);
     }
     return(STAT_OK);
 }
@@ -1794,16 +1850,16 @@ static int bitOr(void)
     DISASM_RT(PC-1, 1);
     STACKDUMP(2, 3);
 
-    PEEK(rightVal, 0)
+    PEEK(rightVal, 0);
     if (rightVal.tag == ARRAY_TAG) {
-        PEEK(leftVal, 1)
+        PEEK(leftVal, 1);
         if (leftVal.tag == ARRAY_TAG) {
             SparseArrayEntry *leftIter, *rightIter;
             resultArray.tag = ARRAY_TAG;
             resultArray.val.arrayPtr = ArrayNew();
             
-            POP(rightVal)
-            POP(leftVal)
+            POP(rightVal);
+            POP(leftVal);
             leftIter = arrayIterateFirst(&leftVal);
             rightIter = arrayIterateFirst(&rightVal);
             while (leftIter || rightIter) {
@@ -1836,33 +1892,33 @@ static int bitOr(void)
                     return(execError("array insertion failure", NULL));
                 }
             }
-            PUSH(resultArray)
+            PUSH(resultArray);
         }
         else {
             return(execError("can't mix math with arrays and non-arrays", NULL));
         }
     }
     else {
-        POP_INT(n2)
-        POP_INT(n1)
-        PUSH_INT(n1 | n2)
+        POP_INT(n2);
+        POP_INT(n1);
+        PUSH_INT(n1 | n2);
     }
     return(STAT_OK);
 }
 
 static int and(void)
 { 
-    BINARY_NUMERIC_OPERATION(&&)
+    BINARY_NUMERIC_OPERATION(&&);
 }
 
 static int or(void)
 {
-    BINARY_NUMERIC_OPERATION(||)
+    BINARY_NUMERIC_OPERATION(||);
 }
     
 static int not(void)
 {
-    UNARY_NUMERIC_OPERATION(!)
+    UNARY_NUMERIC_OPERATION(!);
 }
 
 /*
@@ -1877,8 +1933,8 @@ static int power(void)
     DISASM_RT(PC-1, 1);
     STACKDUMP(2, 3);
 
-    POP_INT(n2)
-    POP_INT(n1)
+    POP_INT(n2);
+    POP_INT(n1);
     /*  We need to round to deal with pow() giving results slightly above
         or below the real result since it deals with floating point numbers.
         Note: We're not really wanting rounded results, we merely
@@ -1906,7 +1962,7 @@ static int power(void)
             n3 = (int)(pow((double)n1, (double)n2) + (double)0.5);
         }
     }
-    PUSH_INT(n3)
+    PUSH_INT(n3);
     return errCheck("exponentiation");
 }
 
@@ -1923,14 +1979,14 @@ static int concat(void)
     DISASM_RT(PC-1, 1);
     STACKDUMP(2, 3);
 
-    POP_STRING(s2)
-    POP_STRING(s1)
+    POP_STRING(s2);
+    POP_STRING(s1);
     len1 = strlen(s1);
     len2 = strlen(s2);
     out = AllocString(len1 + len2 + 1);
     strncpy(out, s1, len1);
     strcpy(&out[len1], s2);
-    PUSH_STRING(out, len1 + len2)
+    PUSH_STRING(out, len1 + len2);
     return STAT_OK;
 }
 
@@ -1985,7 +2041,7 @@ static int callSubroutine(void)
     	    if (result.tag == NO_TAG) {
     	    	return execError("%s does not return a value", sym->name);
             }
-    	    PUSH(result);
+            PUSH(result);
 	    PC++;
     	}
     	return PreemptRequest ? STAT_PREEMPT : STAT_OK;
@@ -2035,7 +2091,7 @@ static int callSubroutine(void)
         argList = (String *)XtCalloc(nArgs, sizeof(*argList));
 	/* pop arguments off the stack and put them in the argument list */
 	for (i=nArgs-1; i>=0; i--) {
-    	    POP_STRING(argList[i])
+            POP_STRING(argList[i]);
 	}
 
     	/* Call the action routine and check for preemption */
@@ -2090,7 +2146,7 @@ static int returnValOrNone(int valOnStac
 
     /* return value is on the stack */
     if (valOnStack) {
-    	POP(retVal);
+        POP(retVal);
     }
     
     PC = rewindFrame(&FrameP, &StackP);
@@ -2098,13 +2154,13 @@ static int returnValOrNone(int valOnStac
     /* push returned value, if requsted */
     if (PC == NULL) {
 	if (valOnStack) {
-    	    PUSH(retVal);
+            PUSH(retVal);
 	} else {
-	    PUSH(noValue);
+            PUSH(noValue);
 	}
     } else if (PC->func == fetchRetVal) {
 	if (valOnStack) {
-    	    PUSH(retVal);
+            PUSH(retVal);
 	    PC++;
 	} else {
 	    return execError(
@@ -2148,7 +2204,7 @@ static int branchTrue(void)
     DISASM_RT(PC-1, 2);
     STACKDUMP(1, 3);
 
-    POP_INT(value)
+    POP_INT(value);
     addr = PC + PC->value;
     PC++;
     
@@ -2164,7 +2220,7 @@ static int branchFalse(void)
     DISASM_RT(PC-1, 2);
     STACKDUMP(1, 3);
 
-    POP_INT(value)
+    POP_INT(value);
     addr = PC + PC->value;
     PC++;
     
@@ -2243,7 +2299,7 @@ static int makeArrayKeyFromArgs(int nArg
 
     keyLength = sepLen * (nArgs - 1);
     for (i = nArgs - 1; i >= 0; --i) {
-        PEEK(tmpVal, i)
+        PEEK(tmpVal, i);
         if (tmpVal.tag == INT_TAG) {
             keyLength += TYPE_INT_STR_SIZE(tmpVal.val.n);
         }
@@ -2260,7 +2316,7 @@ static int makeArrayKeyFromArgs(int nArg
         if (i != nArgs - 1) {
             strcat(*keyString, ARRAY_DIM_SEP);
         }
-        PEEK(tmpVal, i)
+        PEEK(tmpVal, i);
         if (tmpVal.tag == INT_TAG) {
             sprintf(&((*keyString)[strlen(*keyString)]), "%d", tmpVal.val.n);
         }
@@ -2273,7 +2329,7 @@ static int makeArrayKeyFromArgs(int nArg
     }
     if (!leaveParams) {
         for (i = nArgs - 1; i >= 0; --i) {
-            POP(tmpVal)
+            POP(tmpVal);
         }
     }
     return(STAT_OK);
@@ -2497,12 +2553,12 @@ static int arrayRef(void)
             return(errNum);
         }
 
-        POP(srcArray)
+        POP(srcArray);
         if (srcArray.tag == ARRAY_TAG) {
             if (!ArrayGet(&srcArray, keyString, &valueItem)) {
                 return(execError("referenced array value not in array: %s", keyString));
             }
-            PUSH(valueItem)
+            PUSH(valueItem);
             return(STAT_OK);
         }
         else {
@@ -2510,9 +2566,9 @@ static int arrayRef(void)
         }
     }
     else {
-        POP(srcArray)
+        POP(srcArray);
         if (srcArray.tag == ARRAY_TAG) {
-            PUSH_INT(ArraySize(&srcArray))
+            PUSH_INT(ArraySize(&srcArray));
             return(STAT_OK);
         }
         else {
@@ -2543,14 +2599,14 @@ static int arrayAssign(void)
     STACKDUMP(nDim, 3);
 
     if (nDim > 0) {
-        POP(srcValue)
+        POP(srcValue);
 
         errNum = makeArrayKeyFromArgs(nDim, &keyString, 0);
         if (errNum != STAT_OK) {
             return(errNum);
         }
         
-        POP(dstArray)
+        POP(dstArray);
 
         if (dstArray.tag != ARRAY_TAG && dstArray.tag != NO_TAG) {
             return(execError("cannot assign array element of non-array", NULL));
@@ -2598,7 +2654,7 @@ static int arrayRefAndAssignSetup(void)
     STACKDUMP(nDim + 1, 3);
 
     if (binaryOp) {
-        POP(moveExpr)
+        POP(moveExpr);
     }
     
     if (nDim > 0) {
@@ -2607,14 +2663,14 @@ static int arrayRefAndAssignSetup(void)
             return(errNum);
         }
 
-        PEEK(srcArray, nDim)
+        PEEK(srcArray, nDim);
         if (srcArray.tag == ARRAY_TAG) {
             if (!ArrayGet(&srcArray, keyString, &valueItem)) {
                 return(execError("referenced array value not in array: %s", keyString));
             }
-            PUSH(valueItem)
+            PUSH(valueItem);
             if (binaryOp) {
-                PUSH(moveExpr)
+                PUSH(moveExpr);
             }
             return(STAT_OK);
         }
@@ -2651,7 +2707,7 @@ static int beginArrayIter(void)
     iterator = PC->sym;
     PC++;
 
-    POP(arrayVal)
+    POP(arrayVal);
     
     if (iterator->type == LOCAL_SYM) {
         iteratorValPtr = &FP_GET_SYM_VAL(FrameP, iterator);
@@ -2763,15 +2819,15 @@ static int inArray(void)
     DISASM_RT(PC-1, 1);
     STACKDUMP(2, 3);
 
-    POP(theArray)
+    POP(theArray);
     if (theArray.tag != ARRAY_TAG) {
         return(execError("operator in on non-array", NULL));
     }
-    PEEK(leftArray, 0)
+    PEEK(leftArray, 0);
     if (leftArray.tag == ARRAY_TAG) {
         SparseArrayEntry *iter;
         
-        POP(leftArray)
+        POP(leftArray);
         inResult = 1;
         iter = arrayIterateFirst(&leftArray);
         while (inResult && iter) {
@@ -2780,12 +2836,12 @@ static int inArray(void)
         }
     }
     else {
-        POP_STRING(keyStr)
+        POP_STRING(keyStr);
         if (ArrayGet(&theArray, keyStr, &theValue)) {
             inResult = 1;
         }
     }
-    PUSH_INT(inResult)
+    PUSH_INT(inResult);
     return(STAT_OK);
 }
 
@@ -2819,7 +2875,7 @@ static int deleteArrayElement(void)
         }
     }
 
-    POP(theArray)
+    POP(theArray);
     if (theArray.tag == ARRAY_TAG) {
         if (nDim > 0) {
             ArrayDelete(&theArray, keyString);
@@ -2891,6 +2947,22 @@ int StringToNum(const char *string, int 
     return True;
 }
 
+
+static const char *tagToStr(enum typeTags tag)
+{
+    switch (tag) {
+        case INT_TAG:
+            return "integer";
+        case STRING_TAG:
+            return "string";
+        case ARRAY_TAG:
+            return "array";
+        case NO_TAG:
+        default:
+            return "no value";
+    }
+}
+
 #ifdef DEBUG_DISASSEMBLER   /* dumping values in disassembly or stack dump */
 static void dumpVal(DataValue dv)
 {
@@ -2917,11 +2989,11 @@ static void dumpVal(DataValue dv)
             }
             break;
         case ARRAY_TAG:
-            printf("<array>");
+            printf("<%s>", tagToStr(ARRAY_TAG));
             break;
         case NO_TAG:
             if (!dv.val.inst) {
-                printf("<no value>");
+                printf("<%s>", tagToStr(NO_TAG));
             }
             else {
                 printf("?%8p", dv.val.inst);
