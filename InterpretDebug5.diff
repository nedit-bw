From: Tony Balinski <ajbj@free.fr>
Subject: Provide NEdit Macro stack traces

Avaliable as a patch:

	http://sourceforge.net/tracker/index.php?func=detail&aid=970501&group_id=11005&atid=311005
	[ 970501 ] Provide NEdit Macro stack traces
	InterpretDebug.diff 2004-06-10 11:51

Macro function names are listed when a crash occurs in one of your macros.
The usual error message is followed by a list of the NEdit macro functions
called before getting there. (It doesn't tell you how the macro was invoked
however.) This provides a good clue as to where  a macro programming problem
lies.

Also, debug tracing enhanced to show symbol values in stack traces listed to
terminal output: a boon to interpret.c hackers.

Try changing the definition

	#define STACKDUMP(n, x) stackdump(n, x)

to

	#define STACKDUMP(n, x) stackdump(n, x + 50)

and watching the output of NEdit in an xterm generated while running your
favorite macros!

(You will need to add -DDEBUG_STACK and -DDEBUG_ASSEMBLY in your compilation
flags to enable the debug tracing.)

Thanks to Eddy De Greef!

	InterpretDebug2.diff	2004-06-11 17:13

This version passes an extra "name" string to ParseMacro(). This name is
used as a "function name" in the stack dumps, when there is no available
function symbol name available (usually at the top level of invocation from
NEdit's user interface). It allows the user to determine which macro is
being invoked or which file is being interpreted when an error occurs.

---

 source/interpret.c   |  405 ++++++++++++++++++++++++++++++++++++++++++---------
 source/interpret.h   |    2 
 source/macro.c       |    9 -
 source/nedit.c       |    4 
 source/parse.h       |    3 
 source/parse.y       |   10 +
 source/smartIndent.c |   20 +-
 source/userCmds.c    |   49 +++---
 8 files changed, 396 insertions(+), 106 deletions(-)

diff --quilt old/source/interpret.c new/source/interpret.c
--- old/source/interpret.c
+++ new/source/interpret.c
@@ -38,7 +38,9 @@ static const char CVSID[] = "$Id: interp
 #include "text.h"
 #include "rbTree.h"
 
+#include <unistd.h>
 #include <stdio.h>
+#include <stdarg.h>
 #include <stdlib.h>
 #include <string.h>
 #include <math.h>
@@ -104,7 +106,10 @@ static const char *tagToStr(enum typeTag
 
 #if defined(DEBUG_ASSEMBLY) || defined(DEBUG_STACK)
 #define DEBUG_DISASSEMBLER
+static const char *printd(const char *f, ...);
+static int outPrintd();
 static void disasm(Inst *inst, int nInstr);
+static void disasmInternal(Inst *inst, int nInstr);
 #endif /* #if defined(DEBUG_ASSEMBLY) || defined(DEBUG_STACK) */
 
 #ifdef DEBUG_ASSEMBLY   /* for disassembly */
@@ -115,6 +120,7 @@ static void disasm(Inst *inst, int nInst
 
 #ifdef DEBUG_STACK      /* for run-time instruction and stack trace */
 static void stackdump(int n, int extra);
+static void stackdumpInternal(int n, int extra);
 #define STACKDUMP(n, x) stackdump(n, x)
 #define DISASM_RT(i, n) disasm(i, n)
 #else /* #ifndef DEBUG_STACK */
@@ -172,14 +178,18 @@ static int (*OpFns[])() = {
 };
 
 /* Stack-> symN-sym0(FP), argArray, nArgs, oldFP, retPC, argN-arg1, next, ... */
-#define FP_ARG_ARRAY_CACHE_INDEX (-1)
+#define FP_ARG_ARRAY_INDEX (-1)
 #define FP_ARG_COUNT_INDEX (-2)
-#define FP_OLD_FP_INDEX (-3)
-#define FP_RET_PC_INDEX (-4)
-#define FP_PROG_INDEX (-5)
-#define FP_TO_ARGS_DIST (5) /* should be 0 - (above index) */
+#define FP_FUNCTION_NAME   (-3) /* !! */
+#define FP_SYMBOL_TABLE    (-4) /* !! */
+#define FP_OLD_FP_INDEX    (-5)
+#define FP_RET_PC_INDEX    (-6)
+#define FP_PROG_INDEX      (-7)
+
+#define FP_TO_ARGS_DIST (0 - FP_PROG_INDEX) /* should be 0 - (above index) */
+
 #define FP_GET_ITEM(xFrameP,xIndex) (*(xFrameP + xIndex))
-#define FP_GET_ARG_ARRAY_CACHE(xFrameP) (FP_GET_ITEM(xFrameP, FP_ARG_ARRAY_CACHE_INDEX))
+#define FP_GET_ARG_ARRAY(xFrameP) (FP_GET_ITEM(xFrameP, FP_ARG_ARRAY_INDEX))
 #define FP_GET_ARG_COUNT(xFrameP) (FP_GET_ITEM(xFrameP, FP_ARG_COUNT_INDEX).val.n)
 #define FP_GET_OLD_FP(xFrameP) ((FP_GET_ITEM(xFrameP, FP_OLD_FP_INDEX)).val.dataval)
 #define FP_GET_RET_PC(xFrameP) ((FP_GET_ITEM(xFrameP, FP_RET_PC_INDEX)).val.inst)
@@ -267,6 +277,7 @@ Program *FinishCreatingProgram(Accumulat
     newProg->code = (Inst *)XtMalloc(progLen);
     memcpy(newProg->code, Prog, progLen);
     newProg->localSymList = LocalSymList;
+    newProg->name = NULL;
     newProg->refcount = 1;
     
     /* Local variables' values are stored on the stack.  Here we assign
@@ -291,6 +302,7 @@ void FreeProgram(Program *prog)
     if (--prog->refcount == 0) {
         freeSymbolTable(prog->localSymList);
         XtFree((char *)prog->code);
+        XtFree((char *)prog->name);
         XtFree((char *)prog);
     }
 }
@@ -441,7 +453,8 @@ void FillLoopAddrs(Inst *breakAddr, Inst
 ** helper function to setup the next frame
 */
 static void setupFrame(DataValue **frameP, DataValue **stackP,
-        Inst **pc, Program *prog, int nArgs, DataValue *args)
+        Inst **pc, Program *prog, int nArgs, DataValue *args,
+        const char *name)
 {
     static DataValue noValue = {NO_TAG, {0}};
     int i;
@@ -475,6 +488,17 @@ static void setupFrame(DataValue **frame
     (*stackP)->val.dataval = *frameP;
     (*stackP)++;
 
+    /* symbol table */
+    (*stackP)->tag = NO_TAG;
+    (*stackP)->val.sym = prog->localSymList;
+    (*stackP)++;
+
+    /* macro name */
+    (*stackP)->tag = STRING_TAG;
+    (*stackP)->val.str.rep = (char *)name;
+    (*stackP)->val.str.len = strlen(name);
+    (*stackP)++;
+
     /* nArgs */
     (*stackP)->tag = NO_TAG;
     (*stackP)->val.n = nArgs;
@@ -551,7 +575,7 @@ int ExecuteMacro(WindowInfo *window, Pro
     /* prog will be freed by cller, but by stack also, so inc refcount */
     prog->refcount++;
     setupFrame(&context->frameP, &context->stackP, &context->pc,
-            prog, nArgs, args);
+            prog, nArgs, args, prog->name ? prog->name : "<exec-macro>");
     
     /* Begin execution, return on error or preemption */
     return ContinueMacro(context, result, msg);
@@ -627,7 +651,8 @@ int ContinueMacro(RestartData *continuat
 */
 void RunMacroAsSubrCall(Program *prog)
 {
-    setupFrame(&FrameP, &StackP, &PC, prog, 0, NULL);
+    setupFrame(&FrameP, &StackP, &PC, prog, 0, NULL,
+            prog->name ? prog->name : "<run-macro>");
 }
 
 void FreeRestartData(RestartData *context)
@@ -1419,7 +1444,7 @@ static int pushArgArray(void)
     STACKDUMP(0, 3);
 
     nArgs = FP_GET_ARG_COUNT(FrameP);
-    resultArray = &FP_GET_ARG_ARRAY_CACHE(FrameP);
+    resultArray = &FP_GET_ARG_ARRAY(FrameP);
     if (resultArray->tag != ARRAY_TAG) {
         resultArray->tag = ARRAY_TAG;
         resultArray->val.arrayPtr = ArrayNew();
@@ -2187,7 +2212,7 @@ static int callSubroutine(void)
         prog = sym->value.val.prog;
         prog->refcount++;
         /* -nArgs means 'arguments are on stack' */
-        setupFrame(&FrameP, &StackP, &PC, prog, -nArgs, NULL);
+        setupFrame(&FrameP, &StackP, &PC, prog, -nArgs, NULL, sym->name);
    	return STAT_OK;
     }
     
@@ -2713,7 +2738,7 @@ static int arrayRef(void)
     PC++;
 
     DISASM_RT(PC-2, 2);
-    STACKDUMP(nDim, 3);
+    STACKDUMP(nDim+1, 3);
 
     if (nDim > 0) {
         errNum = makeArrayKeyFromArgs(nDim, &keyString, 0);
@@ -2764,7 +2789,7 @@ static int arrayAssign(void)
     PC++;
 
     DISASM_RT(PC-2, 1);
-    STACKDUMP(nDim, 3);
+    STACKDUMP(nDim+2, 3);
 
     if (nDim > 0) {
         POP(srcValue);
@@ -2802,9 +2827,9 @@ static int arrayAssign(void)
 ** for use with assign-op operators (eg a[i,j] += k
 **
 ** Before: Prog->  [binOp], nDim, next, ...
-**         TheStack-> [rhs], indnDim, ... ind1, next, ...
+**         TheStack-> [rhs], indnDim, ... ind1, ArraySym, next, ...
 ** After:  Prog->  binOp, nDim, [next], ...
-**         TheStack-> [rhs], arrayValue, next, ...
+**         TheStack-> [rhs], arrayValue, ArraySym, next, ...
 */
 static int arrayRefAndAssignSetup(void)
 {
@@ -2819,7 +2844,7 @@ static int arrayRefAndAssignSetup(void)
     PC++;
 
     DISASM_RT(PC-3, 3);
-    STACKDUMP(nDim + 1, 3);
+    STACKDUMP(nDim + (binaryOp ? 2 : 1), 3);
 
     if (binaryOp) {
         POP(moveExpr);
@@ -3073,6 +3098,59 @@ static int errCheck(const char *s)
 }
 
 /*
+** build a stack dump string, reallocating s as necessary.
+*/
+static char *stackDumpStr(DataValue *fp, const char *msg, char **s, int *pLen)
+{
+    int len;
+    const char *op;
+    char *np;
+    DataValue *nfp = fp;
+
+#ifdef DEBUG_STACK
+    const char *dump;
+    printd("\n\n");
+    disasmInternal(PC - 1, 1);
+    stackdumpInternal(0, 50);
+    dump = printd(NULL);
+#endif
+
+    /* first measure the lengths */
+    len = strlen(msg) + 1;
+    for (nfp = fp; nfp; nfp = FP_GET_OLD_FP(nfp)) {
+        len = len + FP_GET_ITEM(nfp, FP_FUNCTION_NAME).val.str.len + 1;
+    }
+#ifdef DEBUG_STACK
+    len += strlen(dump);
+#endif
+    if (*pLen < len)
+    {
+        *s = *s ? XtRealloc(*s, len) : XtMalloc(len);
+        *pLen = len;
+    }
+    /* now copy */
+    np = *s;
+    op = msg;
+    while (*op)
+        *np++ = *op++;
+
+    for (nfp = fp; nfp; nfp = FP_GET_OLD_FP(nfp)) {
+        *np++ = '\n';
+        op = FP_GET_ITEM(nfp, FP_FUNCTION_NAME).val.str.rep;
+        while (*op)
+            *np++ = *op++;
+    }
+#ifdef DEBUG_STACK
+    op = dump;
+    while (*op)
+        *np++ = *op++;
+#endif
+
+    *np = 0;
+    return *s;
+}
+
+/*
 ** combine two strings in a static area and set ErrMsg to point to the
 ** result.  Returns false so a single return execError() statement can
 ** be used to both process the message and return.
@@ -3080,9 +3158,11 @@ static int errCheck(const char *s)
 static int execError(const char *s1, const char *s2)
 {
     static char msg[MAX_ERR_MSG_LEN];
+    static char *err = NULL;
+    static int errlen = 0;
     
     sprintf(msg, s1, s2);
-    ErrMsg = msg;
+    ErrMsg = stackDumpStr(FrameP, msg, &err, &errlen);
     return STAT_ERROR;
 }
 
@@ -3136,11 +3216,83 @@ static const char *tagToStr(enum typeTag
 }
 
 #ifdef DEBUG_DISASSEMBLER   /* dumping values in disassembly or stack dump */
+static char *printdBuffer = NULL;
+static int printdPos = 0;
+static int printdSize = 0;
+
+static const char *printd(const char *f, ...)
+{
+    char buffer[4096];
+    int len;
+    va_list args;
+
+    if (!f)
+    {
+      printdPos = 0;        /* reset for next time */
+      return printdBuffer;
+    }
+
+    va_start(args, f);
+    vsprintf(buffer, f, args);
+    va_end(args);
+
+    len = strlen(buffer);
+    if (!printdBuffer)
+    {
+        printdSize = 4096;
+        printdBuffer = XtMalloc(printdSize);
+        printdPos = 0;
+    }
+    else
+    {
+        int needSize = printdPos + len + 1;
+        if (needSize > printdSize)
+        {
+            int newSize = printdSize;
+            while (newSize < needSize)
+                newSize *= 2;
+            printdBuffer = XtRealloc(printdBuffer, newSize);
+            printdSize = newSize;
+        }
+    }
+    strcpy(&printdBuffer[printdPos], buffer);
+    printdPos += len;
+
+    return printdBuffer;
+}
+
+int outPrintd()
+{
+    const char *s = printd(NULL);
+    const char *cp;
+
+    static int outIsTTY = -1;
+    if (outIsTTY == -1)
+      outIsTTY = isatty(fileno(stdout));
+
+    if (outIsTTY)
+    {
+        for (cp = s; *cp; cp++)
+            if (*cp == '\n')
+                printf("\033[K\n");
+            else
+                putchar(*cp);
+    }
+    else
+    {
+        for (cp = s; *cp; cp++)
+            putchar(*cp);
+    }
+    return cp - s;
+}
+#endif /* #ifdef DEBUG_DISASSEMBLER */
+
+#ifdef DEBUG_DISASSEMBLER   /* dumping values in disassembly or stack dump */
 static void dumpVal(DataValue dv)
 {
     switch (dv.tag) {
         case INT_TAG:
-            printf("i=%d", dv.val.n);
+            printd("i=%d", dv.val.n);
             break;
         case STRING_TAG:
             {
@@ -3148,38 +3300,39 @@ static void dumpVal(DataValue dv)
                 char s[21];
                 char *src = dv.val.str.rep;
                 if (!src) {
-                    printf("s=<NULL>");
+                    printd("s=<NULL>");
                 }
                 else {
                     for (k = 0; src[k] && k < sizeof s - 1; k++) {
                         s[k] = isprint(src[k]) ? src[k] : '?';
                     }
                     s[k] = 0;
-                    printf("s=\"%s\"%s[%d]", s,
+                    printd("s=\"%s\"%s[%d]", s,
                            src[k] ? "..." : "", strlen(src));
                 }
             }
             break;
         case ARRAY_TAG:
-            printf("<%s>", tagToStr(ARRAY_TAG));
+            printd("%08p <%s>[%d]", dv.val.arrayPtr, tagToStr(ARRAY_TAG),
+                    ArraySize(&dv));
             break;
         case NO_TAG:
             if (!dv.val.inst) {
-                printf("<%s>", tagToStr(NO_TAG));
+                printd("<%s>", tagToStr(NO_TAG));
             }
             else {
-                printf("?%8p", dv.val.inst);
+                printd("?%8p", dv.val.inst);
             }
             break;
         default:
-            printf("UNKNOWN DATA TAG %d ?%8p", dv.tag, dv.val.inst);
+            printd("UNKNOWN DATA TAG %d ?%8p", dv.tag, dv.val.inst);
             break;
     }
 }
 #endif /* #ifdef DEBUG_DISASSEMBLER */
 
 #ifdef DEBUG_DISASSEMBLER /* For debugging code generation */
-static void disasm(Inst *inst, int nInstr)
+static void disasmInternal(Inst *inst, int nInstr)
 {
     static const char *opNames[] = {
 #define OP(name, fn) #name,
@@ -3188,15 +3341,15 @@ static void disasm(Inst *inst, int nInst
     };
     int i, j;
     
-    printf("\n");
+    printd("\n");
     for (i = 0; i < nInstr; ++i) {
-        printf("Prog %8p ", &inst[i]);
+        printd("Prog %8p ", &inst[i]);
         for (j = 0; j < N_OPS; ++j) {
             if (inst[i].func == OpFns[j]) {
-                printf("%22s ", opNames[j]);
+                printd("%22s ", opNames[j]);
                 if (j == OP_PUSH_SYM || j == OP_ASSIGN) {
                     Symbol *sym = inst[i+1].sym;
-                    printf("%s", sym->name);
+                    printd("%s", sym->name);
                     if (sym->value.tag == STRING_TAG &&
                         strncmp(sym->name, "string #", 8) == 0) {
                         dumpVal(sym->value);
@@ -3204,29 +3357,29 @@ static void disasm(Inst *inst, int nInst
                     ++i;
                 }
                 else if (j == OP_PUSH_IMMED) {
-                    printf("i=%d", inst[i+1].value);
+                    printd("i=%d", inst[i+1].value);
                     ++i;
                 }
                 else if (j == OP_BRANCH || j == OP_BRANCH_FALSE ||
                         j == OP_BRANCH_NEVER || j == OP_BRANCH_TRUE) {
-                    printf("to=(%d) %p", inst[i+1].value,
+                    printd("to=(%d) %p", inst[i+1].value,
                             &inst[i+1] + inst[i+1].value);
                     ++i;
                 }
                 else if (j == OP_CONCAT) {
-                    printf("nExpr=%d", inst[i+1].value);
+                    printd("nExpr=%d", inst[i+1].value);
                     ++i;
                 }
                 else if (j == OP_SUBR_CALL) {
-                    printf("%s (%d arg)", inst[i+1].sym->name, inst[i+2].value);
+                    printd("%s (%d arg)", inst[i+1].sym->name, inst[i+2].value);
                     i += 2;
                 }
                 else if (j == OP_BEGIN_ARRAY_ITER) {
-                    printf("%s in", inst[i+1].sym->name);
+                    printd("%s in", inst[i+1].sym->name);
                     ++i;
                 }
                 else if (j == OP_ARRAY_ITER) {
-                    printf("%s = %s++ end-loop=(%d) %p",
+                    printd("%s = %s++ end-loop=(%d) %p",
                             inst[i+1].sym->name,
                             inst[i+2].sym->name,
                             inst[i+3].value, &inst[i+3] + inst[i+3].value);
@@ -3234,68 +3387,182 @@ static void disasm(Inst *inst, int nInst
                 }
                 else if (j == OP_ARRAY_REF || j == OP_ARRAY_DELETE ||
                             j == OP_ARRAY_ASSIGN) {
-                    printf("nDim=%d", inst[i+1].value);
+                    printd("nDim=%d", inst[i+1].value);
                     ++i;
                 }
                 else if (j == OP_ARRAY_REF_ASSIGN_SETUP) {
-                    printf("binOp=%s ", inst[i+1].value ? "true" : "false");
-                    printf("nDim=%d", inst[i+2].value);
+                    printd("binOp=%s ", inst[i+1].value ? "true" : "false");
+                    printd("nDim=%d", inst[i+2].value);
                     i += 2;
                 }
                 else if (j == OP_PUSH_ARRAY_SYM) {
-                    printf("%s", inst[++i].sym->name);
-                    printf(" %s", inst[i+1].value ? "createAndRef" : "refOnly");
+                    printd("%s", inst[++i].sym->name);
+                    printd(" %s", inst[i+1].value ? "createAndRef" : "refOnly");
                     ++i;
                 }
 
-                printf("\n");
+                printd("\n");
                 break;
             }
         }
         if (j == N_OPS) {
-            printf("%x\n", inst[i].value);
+            printd("%x\n", inst[i].value);
         }
     }
 }
+
+static void disasm(Inst *inst, int nInstr)
+{
+    static int outIsTTY = -1;
+    if (outIsTTY == -1) outIsTTY = isatty(fileno(stdout));
+    if (outIsTTY) { printd("\033[H"); }
+    disasmInternal(inst, nInstr);
+    if (outIsTTY) { printd("\033[J\n"); }
+    outPrintd();
+}
 #endif /* #ifdef DEBUG_DISASSEMBLER */
 
 #ifdef DEBUG_STACK  /* for run-time stack dumping */
-#define STACK_DUMP_ARG_PREFIX "Arg"
-static void stackdump(int n, int extra)
-{
-    /* TheStack-> symN-sym1(FP), argArray, nArgs, oldFP, retPC, argN-arg1, next, ... */
-    int nArgs = FP_GET_ARG_COUNT(FrameP);
-    int i, offset;
-    char buffer[sizeof(STACK_DUMP_ARG_PREFIX) + TYPE_INT_STR_SIZE(int)];
-    printf("Stack ----->\n");
-    for (i = 0; i < n + extra; i++) {
-        char *pos = "";
-        DataValue *dv = &StackP[-i - 1];
-        if (dv < TheStack) {
-            printf("--------------Stack base--------------\n");
-            break;
+#define STACK_DUMP_ARG_PREFIX "  $"
+static void stackdumpframe(DataValue *arrow, DataValue *outpt, DataValue *fp,
+    DataValue *sp, char topMark)
+{
+    DataValue *baseF = &FP_GET_ITEM(fp, FP_OLD_FP_INDEX);
+    DataValue *oldFP = baseF ? baseF->val.dataval : NULL;
+    DataValue *arg1 = &FP_GET_ARG_N(fp, 0);
+    DataValue *fnNm = &FP_GET_ITEM(fp, FP_FUNCTION_NAME);
+    DataValue *dv;
+    DataValue *endDv = (arg1 > outpt) ? arg1 : outpt;
+    int nArgs = FP_GET_ARG_COUNT(fp);
+
+    int nSyms;
+    static int symLen = 0;
+    Symbol *syms = FP_GET_ITEM(fp, FP_SYMBOL_TABLE).val.sym;
+    Symbol *sym;
+
+#ifdef DEBUG_STACK_HEADFIRST
+#else
+    /* do caller's frame */
+    if (oldFP || arg1 > endDv)
+        stackdumpframe(arrow, outpt, oldFP, arg1, ' ');
+#endif /* #ifdef DEBUG_STACK_HEADFIRST */
+
+    /* do current frame */
+    /* how many symbols are there? */
+    for (sym = syms, nSyms = 0; sym != NULL; sym = sym->next) {
+        nSyms++;
+        if (symLen < 27) {
+            int len = strlen(sym->name);
+            if (len > 27)
+                len = 27;
+            if (len > symLen)
+                symLen = len;
         }
-        offset = dv - FrameP;
+    }
+
+    /* output instructions between endDv and sp - 1 inclusive */
+#ifdef DEBUG_STACK_HEADFIRST
+    dv = sp;
+    while (--dv >= endDv)
+#else
+    for (dv = endDv; dv < sp; dv++)
+#endif /* #ifdef DEBUG_STACK_HEADFIRST */
+    {
+        const char *posFmt = "%-6s ";
+        const char *symName = "";
 
-        printf("%4.4s", i < n ? ">>>>" : "");
-        printf("%8p ", dv);
+        char *pos = "";
+        char buffer[sizeof(STACK_DUMP_ARG_PREFIX) + TYPE_INT_STR_SIZE(int)];
+        int offset = dv - fp;
+        const char *leadIn = (dv >= arrow) ? ">>>>" :
+                             (dv ==  arg1) ? "----" :
+                             (dv ==  fnNm) ? "====" : "";
+        printd("%4.4s", leadIn);
+        printd("%8p%c", dv, topMark);
         switch (offset) {
-            case 0:                         pos = "FrameP"; break;  /* first local symbol value */
-            case FP_ARG_ARRAY_CACHE_INDEX:  pos = "args";   break;  /* arguments array */
-            case FP_ARG_COUNT_INDEX:        pos = "NArgs";  break;  /* number of arguments */
-            case FP_OLD_FP_INDEX:           pos = "OldFP";  break;
-            case FP_RET_PC_INDEX:           pos = "RetPC";  break;
-            case FP_PROG_INDEX:             pos = "Prog";   break;
+            case FP_ARG_ARRAY_INDEX: pos = "args[]"; break; /* argument array */
+            case FP_ARG_COUNT_INDEX: pos = "NArgs";  break; /* num. arguments */
+            case FP_FUNCTION_NAME:   pos = "FnName"; break;
+            case FP_SYMBOL_TABLE:    pos = "FnSyms"; break;
+            case FP_OLD_FP_INDEX:    pos = "OldFP";  break;
+            case FP_RET_PC_INDEX:    pos = "RetPC";  break;
+            case FP_PROG_INDEX:      pos = "Prog";   break;
             default:
-                if (offset < -FP_TO_ARGS_DIST && offset >= -FP_TO_ARGS_DIST - nArgs) {
+                if (offset < -FP_TO_ARGS_DIST &&
+                    offset >= -FP_TO_ARGS_DIST - nArgs)
+                {
                     sprintf(pos = buffer, STACK_DUMP_ARG_PREFIX "%d",
                             offset + FP_TO_ARGS_DIST + nArgs + 1);
                 }
+                else if (0 <= offset && offset < nSyms) {
+                    sprintf(pos = buffer, offset ? "[%d]" : "FP[%d]", offset);
+                    posFmt = "%6s ";
+                }
+                else if (offset == 0) {
+                    pos = "FrameP";
+                }
                 break;
         }
-        printf("%-6s ", pos);
-        dumpVal(*dv);
-        printf("\n");
+        printd(posFmt, pos);
+
+        /* local symbol names? */
+        if (0 <= offset && offset < nSyms) {
+            for (sym = syms; sym != NULL; sym = sym->next) {
+                if (sym->value.val.n == offset) {
+                    symName = sym->name;
+                    break;
+                }
+            }
+        }
+        printd("%-*.*s ", symLen, symLen, symName);
+
+        if (dv == fnNm && dv->tag == STRING_TAG && dv->val.str.rep)
+            printd("%s", dv->val.str.rep);
+        else
+            dumpVal(*dv);
+
+        printd("\n");
     }
+
+#ifdef DEBUG_STACK_HEADFIRST
+    /* do caller's frame */
+    if (oldFP || arg1 > endDv)
+        stackdumpframe(arrow, outpt, oldFP, arg1, ' ');
+#else
+#endif /* #ifdef DEBUG_STACK_HEADFIRST */
+}
+
+static void stackdumpInternal(int n, int extra)
+{
+    DataValue *arrow = StackP - n;
+    DataValue *outpt = StackP - n - extra;
+
+#ifdef DEBUG_STACK_HEADFIRST
+    printd("Stack ----->\n");
+    stackdumpframe(arrow, outpt, FrameP, StackP, '*');
+    if (outpt < TheStack)
+        printd("--------------Stack base--------------\n");
+#else
+    if (outpt < TheStack)
+        printd("--------------Stack base--------------\n");
+    stackdumpframe(arrow, outpt, FrameP, StackP, '*');
+    printd("Stack ----->\n");
+#endif /* #ifdef DEBUG_STACK_HEADFIRST */
 }
+
+static void stackdump(int n, int extra)
+{
+    static int outIsTTY = -1;
+    if (outIsTTY == -1)
+        outIsTTY = isatty(fileno(stdout));
+
+    stackdumpInternal(n, extra);
+
+    if (outIsTTY)
+        printd("\033[J\n");
+
+    outPrintd();
+    fflush(stdout);
+}
+
 #endif /* ifdef DEBUG_STACK */
diff --quilt old/source/interpret.h new/source/interpret.h
--- old/source/interpret.h
+++ new/source/interpret.h
@@ -86,6 +86,7 @@ typedef struct DataValueTag {
         Inst* inst;
         struct DataValueTag* dataval;
         struct SparseArrayEntryTag *arrayPtr;
+        struct SymbolRec *sym;
     } val;
 } DataValue;
 
@@ -107,6 +108,7 @@ typedef struct ProgramTag {
     Symbol *localSymList;
     Inst *code;
     unsigned refcount;
+    char *name;
 } Program;
 
 /* Information needed to re-start a preempted macro */
diff --quilt old/source/macro.c new/source/macro.c
--- old/source/macro.c
+++ new/source/macro.c
@@ -546,7 +546,8 @@ void Replay(WindowInfo *window)
             window->macroCmdData == NULL) {
         /* Parse the replay macro (it's stored in text form) and compile it into
         an executable program "prog" */
-        prog = ParseMacro(ReplayMacro, &errMsg, &stoppedAt, False);
+        prog = ParseMacro(ReplayMacro, &errMsg, &stoppedAt, False,
+                "replay macro");
         if (prog == NULL) {
             fprintf(stderr,
                 "NEdit internal error, learn/replay macro syntax error: %s\n",
@@ -644,7 +645,7 @@ static int readCheckMacroString(Widget d
     char *stoppedAt, *errMsg;
     Program *prog;
 
-    prog = ParseMacro(string, &errMsg, &stoppedAt, True);
+    prog = ParseMacro(string, &errMsg, &stoppedAt, True, errIn);
     if (prog == NULL) {
         if (errPos != NULL) {
             *errPos = stoppedAt;
@@ -926,7 +927,7 @@ void DoMacro(WindowInfo *window, const c
     tMacro[macroLen+1] = '\0';
     
     /* Parse the macro and report errors if it fails */
-    prog = ParseMacro(tMacro, &errMsg, &stoppedAt, False);
+    prog = ParseMacro(tMacro, &errMsg, &stoppedAt, False, errInName);
     if (prog == NULL) {
     	ParseError(window->shell, tMacro, stoppedAt, errInName, errMsg);
 	XtFree(tMacro);
@@ -1190,7 +1191,7 @@ selEnd += $text_length - startLength\n}\
 	sprintf(loopedCmd, loopMacro, how, command);
     
     /* Parse the resulting macro into an executable program "prog" */
-    prog = ParseMacro(loopedCmd, &errMsg, &stoppedAt, False);
+    prog = ParseMacro(loopedCmd, &errMsg, &stoppedAt, False, "repeat macro");
     if (prog == NULL) {
 	fprintf(stderr, "NEdit internal error, repeat macro syntax wrong: %s\n",
     		errMsg);
diff --quilt old/source/nedit.c new/source/nedit.c
--- old/source/nedit.c
+++ new/source/nedit.c
@@ -832,10 +832,10 @@ static int checkDoMacroArg(const char *m
     tMacro[macroLen+1] = '\0';
     
     /* Do a test parse */
-    prog = ParseMacro(tMacro, &errMsg, &stoppedAt, False);
+    prog = ParseMacro(tMacro, &errMsg, &stoppedAt, False, "-do macro");
     XtFree(tMacro);
     if (prog == NULL) {
-    	ParseError(NULL, tMacro, stoppedAt, "argument to -do", errMsg);
+    	ParseError(NULL, tMacro, stoppedAt, "-do macro", errMsg);
 	return False;
     }
     FreeProgram(prog);
diff --quilt old/source/parse.h new/source/parse.h
--- old/source/parse.h
+++ new/source/parse.h
@@ -30,6 +30,7 @@
 
 #include "interpret.h"
 
-Program *ParseMacro(char *expr, char **msg, char **stoppedAt, int allowDefine);
+Program *ParseMacro(char *expr, char **msg, char **stoppedAt, int allowDefine,
+        const char *name);
 
 #endif /* NEDIT_PARSE_H_INCLUDED */
diff --quilt old/source/parse.y new/source/parse.y
--- old/source/parse.y
+++ new/source/parse.y
@@ -632,10 +632,12 @@ blank:  /* nothing */
 ** as a pointer to a static string in msg, and the length of the string up
 ** to where parsing failed in stoppedAt.
 */
-Program *ParseMacro(char *expr, char **msg, char **stoppedAt, int allowDefine)
+Program *ParseMacro(char *expr, char **msg, char **stoppedAt, int allowDefine,
+        const char *name)
 {
     Program *prog;
     AccumulatorData *acc = XtNew(AccumulatorData);
+    static const char *prefix = ">> ";
 
 #if YYDEBUG
     int oldyydebug = yydebug;
@@ -670,6 +672,12 @@ Program *ParseMacro(char *expr, char **m
     prog = FinishCreatingProgram(acc);
     XtFree((char *)acc);
 
+    if (!name)
+        name = "--unknown--";
+
+    prog->name = XtMalloc(strlen(name) + strlen(prefix) + 1);
+    strcat(strcpy(prog->name, prefix), name);
+
     /* parse succeeded */
     *msg = "";
     *stoppedAt = InPtr;
diff --quilt old/source/smartIndent.c new/source/smartIndent.c
--- old/source/smartIndent.c
+++ new/source/smartIndent.c
@@ -747,18 +747,18 @@ void BeginSmartIndent(WindowInfo *window
     winData->inNewLineMacro = 0;
     winData->inModMacro = 0;
     winData->newlineMacro = ParseMacro(indentMacros->newlineMacro, &errMsg,
-    	    &stoppedAt, False);
+            &stoppedAt, False, "smart indent newline macro");
     if (winData->newlineMacro == NULL) {
         XtFree((char *)winData);
-    	ParseError(window->shell, indentMacros->newlineMacro, stoppedAt,
-    	    	"newline macro", errMsg);
+        ParseError(window->shell, indentMacros->newlineMacro, stoppedAt,
+                "smart indent newline macro", errMsg);
     	return;
     }
     if (indentMacros->modMacro == NULL)
     	winData->modMacro = NULL;
     else {
     	winData->modMacro = ParseMacro(indentMacros->modMacro, &errMsg,
-    	    	&stoppedAt, False);
+                &stoppedAt, False, "smart indent modify macro");
     	if (winData->modMacro == NULL) {
             FreeProgram(winData->newlineMacro);
             XtFree((char *)winData);
@@ -1415,7 +1415,7 @@ static int checkSmartIndentDialogData(vo
     if (!TextWidgetIsBlank(SmartIndentDialog.initMacro)) {
 	widgetText =ensureNewline(XmTextGetString(SmartIndentDialog.initMacro));
 	if (!CheckMacroString(SmartIndentDialog.shell, widgetText,
-		"initialization macro", &stoppedAt)) {
+                "smart indent initialization macro", &stoppedAt)) {
     	    XmTextSetInsertionPosition(SmartIndentDialog.initMacro,
 		    stoppedAt - widgetText);
 	    XmProcessTraversal(SmartIndentDialog.initMacro, XmTRAVERSE_CURRENT);
@@ -1434,10 +1434,11 @@ static int checkSmartIndentDialogData(vo
     }
 
     widgetText = ensureNewline(XmTextGetString(SmartIndentDialog.newlineMacro));
-    prog = ParseMacro(widgetText, &errMsg, &stoppedAt, False);
+    prog = ParseMacro(widgetText, &errMsg, &stoppedAt, False,
+            "smart indent newline macro");
     if (prog == NULL) {
  	ParseError(SmartIndentDialog.shell, widgetText, stoppedAt,
-    	    	"newline macro", errMsg);
+                "smart indent newline macro", errMsg);
      	XmTextSetInsertionPosition(SmartIndentDialog.newlineMacro,
 		stoppedAt - widgetText);
 	XmProcessTraversal(SmartIndentDialog.newlineMacro, XmTRAVERSE_CURRENT);
@@ -1450,10 +1451,11 @@ static int checkSmartIndentDialogData(vo
     /* Test compile the modify macro */
     if (!TextWidgetIsBlank(SmartIndentDialog.modMacro)) {
     	widgetText = ensureNewline(XmTextGetString(SmartIndentDialog.modMacro));
-    	prog = ParseMacro(widgetText, &errMsg, &stoppedAt, False);
+        prog = ParseMacro(widgetText, &errMsg, &stoppedAt, False,
+                "smart indent modify macro");
 	if (prog == NULL) {
     	    ParseError(SmartIndentDialog.shell, widgetText, stoppedAt,
-    	    	    "modify macro", errMsg);
+                    "smart indent modify macro", errMsg);
      	    XmTextSetInsertionPosition(SmartIndentDialog.modMacro,
 		    stoppedAt - widgetText);
 	    XmProcessTraversal(SmartIndentDialog.modMacro, XmTRAVERSE_CURRENT);
diff --quilt old/source/userCmds.c new/source/userCmds.c
--- old/source/userCmds.c
+++ new/source/userCmds.c
@@ -241,6 +241,8 @@ static Widget BGMenuPasteReplayBtn = NUL
 static void editMacroOrBGMenu(WindowInfo *window, int dialogType);
 static void dimSelDepItemsInMenu(Widget menuPane, menuItemRec **menuList,
 	int nMenuItems, int sensitive);
+static int doMacroMenuCmd(WindowInfo *window, const char *itemName,
+        menuItemRec **menu, int nMenu, const char *menuName);
 static void rebuildMenuOfAllWindows(int menuType);
 static void rebuildMenu(WindowInfo *window, int menuType);
 static Widget findInMenuTree(menuTreeItem *menuTree, int nTreeEntries,
@@ -1260,30 +1262,37 @@ int DoNamedShellMenuCmd(WindowInfo *wind
 ** with menu item name "itemName".  Returns True on successs and False on
 ** failure.
 */
-int DoNamedMacroMenuCmd(WindowInfo *window, const char *itemName)
+static int doMacroMenuCmd(WindowInfo *window, const char *itemName,
+        menuItemRec **menu, int nMenu, const char *menuName)
 {
     int i;
-    
-    for (i=0; i<NMacroMenuItems; i++) {
-    	if (!strcmp(MacroMenuItems[i]->name, itemName)) {
-    	    DoMacro(window, MacroMenuItems[i]->cmd, "macro menu command");
-    	    return True;
-    	}
+    char *name = NULL;
+    Boolean result = False;
+
+    for (i = 0; i < nMenu; i++) {
+        if (!strcmp(menu[i]->name, itemName)) {
+            name = XtMalloc(strlen(menuName) + strlen(itemName) + 1);
+            strcat(strcpy(name, menuName), itemName);
+            DoMacro(window, menu[i]->cmd, name);
+            result = True;
+        }
     }
-    return False;
+    if (name) {
+        XtFree(name);
+    }
+    return result;
+}
+
+int DoNamedMacroMenuCmd(WindowInfo *window, const char *itemName)
+{
+    return doMacroMenuCmd(window, itemName, MacroMenuItems, NMacroMenuItems,
+                          "macro-menu>");
 }
 
 int DoNamedBGMenuCmd(WindowInfo *window, const char *itemName)
 {
-    int i;
-    
-    for (i=0; i<NBGMenuItems; i++) {
-    	if (!strcmp(BGMenuItems[i]->name, itemName)) {
-    	    DoMacro(window, BGMenuItems[i]->cmd, "background menu macro");
-    	    return True;
-    	}
-    }
-    return False;
+    return doMacroMenuCmd(window, itemName, BGMenuItems, NBGMenuItems,
+                          "background-menu>");
 }
 
 /*
@@ -2080,7 +2089,7 @@ static int checkMacroText(char *macro, W
     Program *prog;
     char *errMsg, *stoppedAt;
 
-    prog = ParseMacro(macro, &errMsg, &stoppedAt, False);
+    prog = ParseMacro(macro, &errMsg, &stoppedAt, False, "macro");
     if (prog == NULL) {
 	if (errorParent != NULL) {
 	    ParseError(errorParent, macro, stoppedAt, "macro", errMsg);
@@ -2092,7 +2101,7 @@ static int checkMacroText(char *macro, W
     FreeProgram(prog);
     if (*stoppedAt != '\0') {
 	if (errorParent != NULL) {
-	    ParseError(errorParent, macro, stoppedAt,"macro","syntax error");
+            ParseError(errorParent, macro, stoppedAt, "macro", "syntax error");
     	    XmTextSetInsertionPosition(errFocus, stoppedAt - macro);
 	    XmProcessTraversal(errFocus, XmTRAVERSE_CURRENT);
 	}
@@ -3021,7 +3030,7 @@ static char *copyMacroToEnd(char **inPtr
     }
 
     /* Parse the input */
-    prog = ParseMacro(*inPtr, &errMsg, &stoppedAt, False);
+    prog = ParseMacro(*inPtr, &errMsg, &stoppedAt, False, "macro menu item");
     if (prog == NULL) {
     	ParseError(NULL, *inPtr, stoppedAt, "macro menu item", errMsg);
     	return NULL;
