Subject: extend fn(=array) syntax to fn(..., =array)

All non-negative one-dimensional numerical keys are appended to the
previously arguments, in order. All other entries are merge into the named
argument array.

---

 source/interpret.c |  162 +++++++++++++++++++++++++++--------------------------
 source/ops.h       |    3 
 source/parse.y     |   30 +++++++++
 3 files changed, 114 insertions(+), 81 deletions(-)

diff --quilt old/source/interpret.c new/source/interpret.c
--- old/source/interpret.c
+++ new/source/interpret.c
@@ -2501,7 +2501,7 @@ static int concat(void)
 ** Before: Prog->  [subrSym], nArgs, next, ...
 **         TheStack-> argArray?, argN-arg1, next, ...
 **
-** For callSubroutineStackedN:
+** For callSubroutineUnpackArray:
 ** Before: Prog->  [subrSym], next, ...
 **         TheStack-> nArgs, argArray, argN-arg1, next, ...
 **
@@ -2642,37 +2642,92 @@ static int callSubroutine(void)
 }
 
 /*
-** Before: Prog->  [subrSym], next, ...
-**         Stack-> nArgs, argArray, argN-arg1, next, ...
+** For special call style where the $args array in the called function is
+** assigned from an array in the caller (as "calledFunc(=argsArray)"),
+** take consecutive elements indexed from 1 and put them on the stack, leaving
+** a copy of the actual array at the top of the stack, with the stacked
+** arguments removed. Finally, add the negative of the number of arguments
+** aplus 1 (for the argArray itself). This operation must be followed
+** by OP_SUBR_CALL_UNPACK_ARRAY (callSubroutineUnpackArray()).
 **
-** After:  Prog->  next, ...            -- (built-in called subr)
-**         Stack-> retVal?, next, ...
-**    or:  Prog->  (in called)next, ... -- (macro code called subr)
-**         Stack-> symN-sym1(FP), nArgs, oldFP, retPC, argArray, argN-arg1, next, ...
+** The array copy is needed because if/when the $args array is accessed, the
+** arguments are copied back to the array, probably in different positions, as
+** is the case of a "call(=array)" function, where the first argument is
+** removed (the function name) and the others shifted down once. Without a
+** copy, this modifies the original array - a pass by reference not allowed in
+** the language.
+**
+** Before: Prog->  [sym], nArgs, next, ...
+**         TheStack-> argArray(, namedArgsArray), argN-arg1, next, ...
+** After:  Prog->  sym, nArgs, [next], ...
+**         TheStack-> namedArgsArray, argM-arg1, next, ...
 */
-static int callSubroutineStackedN(void)
+static int callSubroutineUnpackArray(void)
 {
     Symbol *sym;
-    int nArgs;
-    /* this is much like callSubroutine, but we get nArgs off the stack
-       and it will always be negative since there is always an argArray */
+    int nArgs, i, res, haveNamedArgs = 0;
+    SparseArrayEntry *iter;
+    DataValue dvEntry, dvArray, argArray;
+
+    DISASM_RT(PC-1, 1);
 
     sym = PC++->sym;
+    nArgs = PC++->value;
 
-    PEEK_INT(nArgs, 0);
-    DISASM_RT(PC-2, 2);
-    STACKDUMP(-nArgs + 1, 3);   /* +1 for stacked nArgs */
+    if (nArgs < 0) {
+        haveNamedArgs = 1;
+        nArgs = -nArgs - 1;
+    }
 
-    POP_INT(nArgs);
+    STACKDUMP(nArgs + haveNamedArgs + 1, 3);
 
-    assert (nArgs < 0);
+    POP(argArray);
 
-    if (nArgs >= 0) {
-        /* should never happen */
-        return execError("array argument call to %s erroneous", sym->name);
+    if (argArray.tag != ARRAY_TAG) {
+        return execError("argument array call made with non-array value", NULL);
     }
 
-    return callSubroutineFromSymbol(sym, nArgs);
+    if (haveNamedArgs) {
+        POP(dvArray);
+    }
+    else {
+        dvArray.tag = ARRAY_TAG;
+        dvArray.val.arrayPtr = ArrayNew();
+    }
+
+    iter = arrayIterateFirst(&argArray);
+    while (iter) {
+        int thisKey;
+
+        if (iter->value.tag == ARRAY_TAG) {
+            int errNum;
+            DataValue tmpArray;
+
+            errNum = ArrayCopy(&dvEntry, &iter->value);
+            if (errNum != STAT_OK) {
+                return(errNum);
+            }
+        }
+        else {
+            dvEntry = iter->value;
+        }
+
+        if (StringToNum(iter->key, &thisKey) && thisKey >= 0) {
+            PUSH(dvEntry);
+            nArgs++;
+        }
+        else {
+            if (!ArrayInsert(&dvArray, iter->key, &dvEntry)) {
+                return(execError("array copy failed", NULL));
+            }
+        }
+
+        iter = arrayIterateNext(iter);
+    }
+
+    PUSH(dvArray);
+
+    return callSubroutineFromSymbol(sym, -nArgs - 1);
 }
 
 /*
@@ -2732,60 +2787,6 @@ int OverlayRoutineFromProg(Program *prog
 }
 
 /*
-** For special call style where the $args array in the called function is
-** assigned from an array in the caller (as "calledFunc(=argsArray)"),
-** take consecutive elements indexed from 1 and put them on the stack, leaving
-** a copy of the actual array at the top of the stack, with the stacked
-** arguments removed. Finally, add the negative of the number of arguments
-** aplus 1 (for the argArray itself). This operation must be followed
-** by OP_SUBR_CALL_STACKED_N (callSubroutineStackedN()).
-**
-** The array copy is needed because if/when the $args array is accessed, the
-** arguments are copied back to the array, probably in different positions, as
-** is the case of a "call(=array)" function, where the first argument is
-** removed (the function name) and the others shifted down once. Without a
-** copy, this modifies the original array - a pass by reference not allowed in
-** the language.
-**
-** Before: Prog->  next, ...
-**         TheStack-> argArray, next, ...
-** After:  Prog->  next, ...
-**         TheStack-> -(nArgs+1), argArray, argN-arg1, next, ...
-*/
-static int unpackArrayToArgs(void)
-{
-    int nArgs, res;
-
-    DataValue dvEntry, dvArray;
-
-    DISASM_RT(PC-1, 1);
-    STACKDUMP(1, 3);
-
-    POP(dvEntry);
-
-    if (dvEntry.tag != ARRAY_TAG) {
-        return execError("argument array call made with non-array value", NULL);
-    }
-    res = ArrayCopy(&dvArray, &dvEntry);
-    if (res != STAT_OK) {
-        return execError("cannot copy array in array call", NULL);
-    }
-
-    /* push positional argument entries in the array on the stack */
-    for (nArgs = 1; ; ++nArgs) {
-        char *ind = (char *)longAsStr(nArgs);
-        if (!ArrayGet(&dvArray, ind, &dvEntry))
-            break;
-        /* remove them from remaining array */
-        ArrayDelete(&dvArray, ind);
-        PUSH(dvEntry);
-    }
-    PUSH(dvArray);
-    PUSH_INT(-nArgs);
-    return STAT_OK;
-}
-
-/*
 ** This should never be executed, returnVal checks for the presence of this
 ** instruction at the PC to decide whether to push the function's return
 ** value, then skips over it without executing.
@@ -4330,9 +4331,16 @@ static void disasmInternal(Inst *inst, i
                     }
                     i += 2;
                 }
-                else if (j == OP_SUBR_CALL_STACKED_N) {
-                    printd(" %s args[] (?)", inst[i+1].sym->name);
-                    ++i;
+                else if (j == OP_SUBR_CALL_UNPACK_ARRAY) {
+                    int args = inst[i+2].value;
+                    printd(" %s", inst[i+1].sym->name);
+                    if (args < 0) {
+                        printd(" %d+args[] =args[]", -args - 1);
+                    }
+                    else {
+                        printd(" %d args =args[]", args);
+                    }
+                    i += 2;
                 }
                 else if (j == OP_BEGIN_ARRAY_ITER ||
                         j == OP_BEGIN_ARRAY_ITER_ARRAY) {
diff --quilt old/source/parse.y new/source/parse.y
--- old/source/parse.y
+++ new/source/parse.y
@@ -69,6 +69,7 @@ static int follow(char expect, int yes, 
 static int follow2(char expect1, int yes1, char expect2, int yes2, int no);
 static int follow_non_whitespace(char expect, int yes, int no);
 static int eq_look_ahead(void);
+static int comma_look_ahead(void);
 static Symbol *matchesActionRoutine(char **inPtr);
 static int scanString(void);
 
@@ -117,6 +118,7 @@ typedef struct LVinst {
 %token <oper> '=' ADDEQ SUBEQ MULEQ DIVEQ MODEQ ANDEQ OREQ
 %token <oper> INCR DECR
 %type <lvinst> lvlist lventry
+%token ARGSEP
 
 %nonassoc IF_NO_ELSE
 %nonassoc ELSE
@@ -554,9 +556,15 @@ funccall:     TYPEOF '(' {
             }
             | SYMBOL '(' blank '=' blank expr blank ')' {
                 /* a single array replaces the argument list */
-                ADD_OP(OP_UNPACKTOARGS);
-                ADD_OP(OP_SUBR_CALL_STACKED_N);
+                ADD_OP(OP_SUBR_CALL_UNPACK_ARRAY);
                 ADD_SYM(PromoteToGlobal($1));
+                ADD_IMMED(0); /* zero arguments */
+            }
+            | SYMBOL '(' fnarglist ARGSEP blank '=' blank expr blank ')' {
+                /* a single array replaces the argument list */
+                ADD_OP(OP_SUBR_CALL_UNPACK_ARRAY);
+                ADD_SYM(PromoteToGlobal($1));
+                ADD_IMMED($3);
             }
             ;
 
@@ -991,6 +999,7 @@ static int yylex(void)
                         ++InPtr;
                     return result;  /* but return what we started with */
                 }
+    case ',':   return comma_look_ahead();
     default:    return *(InPtr-1);
     }
 }
@@ -1075,6 +1084,23 @@ static int eq_look_ahead(void)
     return '=';
 }
 
+static int comma_look_ahead(void)
+{
+    char *savedInPtr = InPtr;
+
+    /* skip any whitespace */
+    skipWhitespace();
+
+    /* '=' from array argument */
+    if (*InPtr == '=') {
+        InPtr = savedInPtr;
+        return ARGSEP;
+    }
+
+    InPtr = savedInPtr;
+    return ',';
+}
+
 /*
 ** Look (way) ahead for hyphenated routine names which begin at inPtr.  A
 ** hyphenated name is allowed if it is pre-defined in the global symbol
diff --quilt old/source/ops.h new/source/ops.h
--- old/source/ops.h
+++ new/source/ops.h
@@ -60,7 +60,6 @@ OP(ANONARRAY_CLOSE,        anonArrayClos
 OP(NAMED_ARG1,             namedArg1)        /* N */ /* pop(v,kN..k1), a=ary(), a[k1..kN]=v, push(a) */
 OP(NAMED_ARGN,             namedArgN)        /* N */ /* pop(v,kN..k1,a), a[k1..kN]=v, push(a) */
 OP(SWAP_TOP2,              swapTop2)                 /* pop(v1,v2), push(v1,v2) */
-OP(SUBR_CALL_STACKED_N,    callSubroutineStackedN) /*s*/ /* pop(N,a,pN..p1), call(s) */
-OP(UNPACKTOARGS,           unpackArrayToArgs)        /* pop(a), push(a[1]..a[N],a,-(N+1)) */
+OP(SUBR_CALL_UNPACK_ARRAY, callSubroutineUnpackArray) /*s,N*/ /* pop(a), push(numerics(a)), call(s) */
 OP(TYPEOF_IN,              typeOfIn)                 /* enable typeof() */
 OP(TYPEOF_OUT,             typeOfOut)                /* pop(v), push(typeof(v)) */
