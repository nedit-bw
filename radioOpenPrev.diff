From: Tony Balinski <ajbj@free.fr>
Subject: Display the File->Open Previous menu using radio buttons

The radio buttons indicate which files are currently open.

The rebuilding of the Open Previous menu is also performed when a document
is closed, to allow the refresh of "torn-off" copies of this menu. This is
why the invalidatePrevOpenMenus() function was made public rather than
static.

---

 source/menu.c   |   36 ++++++++++++++++++++++++------------
 source/menu.h   |    1 +
 source/window.c |    1 +
 3 files changed, 26 insertions(+), 12 deletions(-)

diff --quilt old/source/menu.c new/source/menu.c
--- old/source/menu.c
+++ new/source/menu.c
@@ -372,7 +372,6 @@ static Widget createMenuRadioToggle(Widg
 	char mnemonic, menuCallbackProc callback, void *cbArg, int set,
 	int mode);
 static Widget createMenuSeparator(Widget parent, char *name, int mode);
-static void invalidatePrevOpenMenus(void);
 static void updateWindowMenu(const WindowInfo *window);
 static void updatePrevOpenMenu(WindowInfo *window);
 static void updateTagsFileMenu(WindowInfo *window);
@@ -4715,7 +4714,7 @@ void InvalidateWindowMenus(void)
 ** Since actually changing the menus is slow, they're just marked and updated
 ** when the user pulls one down.
 */
-static void invalidatePrevOpenMenus(void)
+void InvalidatePrevOpenMenus(void)
 {
     WindowInfo *w;
 
@@ -4779,7 +4778,7 @@ void AddToPrevOpenMenu(const char *filen
     	    nameCopy = PrevOpen[i];
     	    memmove(&PrevOpen[1], &PrevOpen[0], sizeof(char *) * i);
     	    PrevOpen[0] = nameCopy;
-    	    invalidatePrevOpenMenus();
+    	    InvalidatePrevOpenMenus();
 	    WriteNEditDB();
     	    return;
     	}
@@ -4799,7 +4798,7 @@ void AddToPrevOpenMenu(const char *filen
     NPrevOpen++;
     
     /* Mark the Previously Opened Files menu as invalid in all windows */
-    invalidatePrevOpenMenus();
+    InvalidatePrevOpenMenus();
 
     /* Undim the menu in all windows if it was previously empty */
     if (NPrevOpen > 0) {
@@ -4974,12 +4973,15 @@ static void updateWindowMenu(const Windo
 */
 static void updatePrevOpenMenu(WindowInfo *window)
 {
+    char filename[MAXPATHLEN];
+    char pathname[MAXPATHLEN];
     Widget btn;
     WidgetList items;
     Cardinal nItems;
     int n, index;
     XmString st1;
     char **prevOpenSorted;
+    Boolean fileIsOpen = False;
 
     /*  Read history file to get entries written by other sessions.  */
     ReadNEditDB();
@@ -5004,11 +5006,15 @@ static void updatePrevOpenMenu(WindowInf
             XtUnmanageChild(items[n]);
             XtDestroyWidget(items[n]);          
         } else {
-            XtVaSetValues(items[n], XmNlabelString,
-                    st1=XmStringCreateSimple(prevOpenSorted[index]), NULL);
-            XtRemoveAllCallbacks(items[n], XmNactivateCallback);
-            XtAddCallback(items[n], XmNactivateCallback,
-                    (XtCallbackProc)openPrevCB, prevOpenSorted[index]);
+            ParseFilename(prevOpenSorted[index], filename, pathname);
+            fileIsOpen = !!FindWindowWithFile(filename, pathname);
+            XtVaSetValues(items[n],
+                XmNlabelString, st1=XmStringCreateSimple(prevOpenSorted[index]),
+                XmNset, fileIsOpen,
+                NULL);
+            XtRemoveAllCallbacks(items[n], XmNvalueChangedCallback);
+            XtAddCallback(items[n], XmNvalueChangedCallback,
+                (XtCallbackProc)openPrevCB, prevOpenSorted[index]);
             XmStringFree(st1);
             index++;
         }
@@ -5016,12 +5022,16 @@ static void updatePrevOpenMenu(WindowInf
     
     /* Add new items for the remaining file names to the menu */
     for (; index<NPrevOpen; index++) {
-        btn = XtVaCreateManagedWidget("win", xmPushButtonWidgetClass,
+        ParseFilename(prevOpenSorted[index], filename, pathname);
+        fileIsOpen = !!FindWindowWithFile(filename, pathname);
+        btn = XtVaCreateManagedWidget("win", xmToggleButtonWidgetClass,
                 window->prevOpenMenuPane, 
                 XmNlabelString, st1=XmStringCreateSimple(prevOpenSorted[index]),
                 XmNmarginHeight, 0,
-                XmNuserData, TEMPORARY_MENU_ITEM, NULL);
-        XtAddCallback(btn, XmNactivateCallback, (XtCallbackProc)openPrevCB, 
+                XmNuserData, TEMPORARY_MENU_ITEM,
+                XmNset, fileIsOpen,
+                XmNindicatorType, XmONE_OF_MANY, NULL);
+        XtAddCallback(btn, XmNvalueChangedCallback, (XtCallbackProc)openPrevCB,
                 prevOpenSorted[index]);
         XmStringFree(st1);
     }
@@ -5512,6 +5522,8 @@ static void openPrevCB(Widget w, char *n
     char *params[1];
     Widget menu = MENU_WIDGET(w);
     
+    XtVaSetValues(w,
+                  XmNset, True, NULL);
     HidePointerOnKeyedEvent(WidgetToWindow(MENU_WIDGET(w))->lastFocus,
             ((XmAnyCallbackStruct *)callData)->event);
     params[0] = name;
diff --quilt old/source/menu.h new/source/menu.h
--- old/source/menu.h
+++ new/source/menu.h
@@ -40,6 +40,7 @@ Widget CreateMenuBar(Widget parent, Wind
 void InstallMenuActions(XtAppContext context);
 XtActionsRec *GetMenuActions(int *nActions);
 void InvalidateWindowMenus(void);
+void InvalidatePrevOpenMenus(void);
 void CheckCloseDim(void);
 void AddToPrevOpenMenu(const char *filename);
 void WriteNEditDB(void);
diff --quilt old/source/window.c new/source/window.c
--- old/source/window.c
+++ new/source/window.c
@@ -1085,6 +1085,7 @@ void CloseWindow(WindowInfo *window)
     /* remove the window from the global window list, update window menus */
     removeFromWindowList(window);
     InvalidateWindowMenus();
+    InvalidatePrevOpenMenus(); /* refresh "is opened" radio buttons here */
     CheckCloseDim(); /* Close of window running a macro may have been disabled. */
 
     /* remove the tab of the closing document from tab bar */
